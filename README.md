# Airport Manager

## Project Description
The Airport Manager application streamlines airport operations by providing an integrated platform to manage flights, crew assignments, and aircraft servicing. With dedicated modules for different aspects of airport management, the system ensures operational efficiency and enhances the coordination of airport activities.

## Microservices Description
### Airplane Module
This module maintains a comprehensive database of aircraft, their maintenance records, service status, and availability for flights.

### Destination Module
Manages information about destinations, including airport details, runway availability, and other related infrastructure data.

### Flight Module
Handles the scheduling, tracking, and status updating of flights. It ensures timely communication of flight details to passengers and crew.

### Steward Module
Responsible for steward management, including rostering, assignment to flights, and monitoring certifications and training records.

## Use Case Diagram
The use case diagram providing an overview of system interactions:
<br/>
<img src="https://plantuml-server.kkeisuke.dev/svg/VPHHRu8m483V-oiklhiC1TGzB1ADoPBniiLdqy69pKenRTKio_xxGAqSqlZsqNwz7dS7SsEvjeTICYPnQy5Me4Mniv0BZPaLbM8yivM6mKgUrnSQJv1onGlK0-06Lgap4g7tbRQTtIHbh0d4LI4HBZ4sJOpDnIfHMVXb00U36JTOHybp-BHuuZe_HrWl0hgzH8aMxumHDTRxdFUCCJNUXR5ktro5i9E3CQ9GJccASidywaRK_S2oEjwkWwskoguTKtknm-px9YT7BYJ_4bBOdwizON-UYZqgLkUogQTM7M7cALL729wx6JHTbLpXmnoSv6BuMdRl-9fsx_JRvWpNjo3ifhj4OuNYpUG-p9XuBbBaITgZnPwyFTh4Cr14wmrLCFLC5JtGdwnWrhpxAeNXyFNydTnmrEAunR35gCMOrO_mrE04Nc3mfZ9voEi2r-knTvqbxkoqnLcBzGYr710U4HuJ3Wb7X6F24yBashe6R8ughtzX_m00.svg"/>

## Class Diagram
The class diagram for our microservices:
<br/>
<img alt="Preview" src="https://plantuml-server.kkeisuke.dev/png/TLHHRvim47xdLrXvgJGfNPfkwiR3L09iovHG2Tf9qpH5Lp29DMCZ-rYKJltlimqbWQLF-BxlkBlluyojHb3OLDppBaWiceegG2Q5zgW9IC1KpK7G-qDDoL-FaCLTl4o-R6wkRqw2TpO8bkdY8Ti4r_FPQVJ1Hl5waGPnkeclFhvtxwqNGHej3HAct-QpcVVijVq-SxRTOOQ0ZNRzil1h73siuiYSetWL_72dC4Z2UDMYosHZcfd3AaWYWpnR9I47hQdsSllighfwKr9GZKmuaSGdqJ5oh5HioyuH6kaULA4DbRN7dx-wUhLrKD5xLb73hcGEF09qiSi0fTWVuA_pdSc6VF6xQ-biy0UkM7sjgfC9Esa2N8SC5HDRX-Io4QWE8v3X286YK5Jh8MZRdoyNoc94M0bIOSQUHWGgA4kMVvUySRCnWGvNZTZ3GNT8hwcpjjDJCgKn-K-KoJo3vb8Wv9WqrIDLGufMmFWGsalrEyjtj6ZuIPr-ZfVlyAgnQ2-4FxWVxHHGG-TlBnN4rkOMiddaRPQPHo0L879wCaI_6IxdG47TbMiGJMbacVqwUdL1KihRQxfZjVQw_PvSJSXqIYQpoyitazvAd-n0aq_Z9FC8ZdidQtllwPcy-MImQ3ufEJDvAD-MIbOctxkbeZ0_a7y0.png"/>.

# How to run the application

## Pre-requisites

1. Docker installed on your machine
2. Maven installed on your machine
3. Java 21 installed on your machine if you want to run the application locally

## Build and run the application using Docker

1. Clone the repository

```
git clone https://gitlab.fi.muni.cz/xsadigz/airport-manager.git
```

2. Run the following command inside the project directory to build the applications:

```
mvn clean install
```

4. Run the following command to build the Docker image:

```
docker-compose up --build
```

5. To stop the application, run the following command:

```
docker-compose down
```

## Service Availability

### Airplane Module
- Swagger UI: http://localhost:8082/swagger-ui.html

### Destination Module
- Swagger UI: http://localhost:8083/swagger-ui.html

### Flight Module
- Swagger UI: http://localhost:8080/swagger-ui.html

### Steward Module
- Swagger UI: http://localhost:8088/swagger-ui.html