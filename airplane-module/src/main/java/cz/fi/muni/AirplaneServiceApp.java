package cz.fi.muni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication

public class AirplaneServiceApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(AirplaneServiceApp.class, args);
    }

}
