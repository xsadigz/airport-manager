package cz.fi.muni.api.responseDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AirplaneResponseDto {

    Integer id;
    String name;
    String type;
    Integer capacity;
    Double range;
    LocalDate maintenanceSchedule;
    Integer yearOfManufacture;
}