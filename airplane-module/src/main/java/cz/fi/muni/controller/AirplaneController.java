package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.AirplaneRequestDto;
import cz.fi.muni.api.responseDto.AirplaneResponseDto;
import cz.fi.muni.facade.IAirplaneFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/airplanes")
public class AirplaneController {

    private static final Logger log = LoggerFactory.getLogger(AirplaneController.class);

    private final IAirplaneFacade airplaneFacade;

    public AirplaneController(IAirplaneFacade airplaneFacade) {
        this.airplaneFacade = airplaneFacade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addAirplane(@RequestBody AirplaneRequestDto requestDto) {
        airplaneFacade.create(requestDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AirplaneResponseDto> getAirplane(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(airplaneFacade.getById(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<AirplaneResponseDto>> getAllAirplanes() {
        log.info("getAllAirplanes");
        return new ResponseEntity<>(airplaneFacade.getAll(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> editAirplane(@PathVariable Integer id, @RequestBody AirplaneRequestDto requestDto) {
        airplaneFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAirplane(@PathVariable Integer id) {
        airplaneFacade.delete(id);
    }

    @PutMapping("/{id}/availability")
    public ResponseEntity<Void> editAirplane(@PathVariable Integer id, @RequestParam("available") boolean available) {
        airplaneFacade.changeAvailability(id, available);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}