package cz.fi.muni.data.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Airplane")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Airplane implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    @ManyToOne
    @JoinColumn(name = "type_id")
    AirplaneType type;
    Integer capacity;
    Double range;
    LocalDate maintenanceSchedule;
    Integer yearOfManufacture;
    Boolean isAvailable;
}