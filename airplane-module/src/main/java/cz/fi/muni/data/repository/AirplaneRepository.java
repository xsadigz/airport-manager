package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.Airplane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AirplaneRepository extends JpaRepository<Airplane, Integer> {
    Optional<Airplane> findByName(String name);

    @Modifying
    @Query("UPDATE Airplane a SET a.isAvailable = :available WHERE a.id = :id")
    void changeAvailability(Integer id,  boolean available);

}