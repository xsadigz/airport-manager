package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.AirplaneType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AirplaneTypeRepository extends JpaRepository<AirplaneType, Integer> {
    Optional<AirplaneType> findByName(String name);
}