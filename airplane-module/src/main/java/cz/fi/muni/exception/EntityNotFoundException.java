package cz.fi.muni.exception;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(Class entity, Object id) {
        super(entity.getSimpleName() + " with ID: " + id + " ");
    }

    public EntityNotFoundException(Class entity) {
        super(entity.getSimpleName());
    }

}
