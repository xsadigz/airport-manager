package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.AirplaneRequestDto;
import cz.fi.muni.api.responseDto.AirplaneResponseDto;
import cz.fi.muni.data.model.Airplane;
import cz.fi.muni.mapper.AirplaneMapper;
import cz.fi.muni.service.AirplaneServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirplaneFacade implements IAirplaneFacade {

    private final AirplaneMapper airplaneMapper;
    private final AirplaneServiceImpl airplaneServiceImpl;

    public AirplaneFacade(AirplaneMapper airplaneMapper, AirplaneServiceImpl airplaneServiceImpl) {
        this.airplaneMapper = airplaneMapper;
        this.airplaneServiceImpl = airplaneServiceImpl;
    }

    @Override
    public void create(AirplaneRequestDto requestDto) {
        Airplane airplane = airplaneMapper.toEntity(requestDto);
        airplaneServiceImpl.create(airplane);
    }

    @Override
    public AirplaneResponseDto getById(Integer id) {
        return airplaneMapper.toResponseDto(airplaneServiceImpl.getById(id));
    }

    @Override
    public List<AirplaneResponseDto> getAll() {
        return airplaneMapper.toResponseDtoList(airplaneServiceImpl.getAll());
    }

    @Override
    public void update(Integer id, AirplaneRequestDto requestDto) {
        Airplane airplane = airplaneMapper.toEntity(requestDto);
        airplaneServiceImpl.update(id, airplane);
    }

    @Override
    public void delete(Integer id) {
        airplaneServiceImpl.delete(id);
    }

    @Override
    public void changeAvailability(Integer id, boolean available) {
        airplaneServiceImpl.changeAvailability(id, available);
    }
}