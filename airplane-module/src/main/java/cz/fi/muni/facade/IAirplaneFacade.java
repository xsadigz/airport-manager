package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.AirplaneRequestDto;
import cz.fi.muni.api.responseDto.AirplaneResponseDto;

import java.util.List;

public interface IAirplaneFacade {

    void create(AirplaneRequestDto requestDto);

    AirplaneResponseDto getById(Integer id);

    List<AirplaneResponseDto> getAll();

    void update(Integer id, AirplaneRequestDto requestDto);

    void delete(Integer id);

    void changeAvailability(Integer id, boolean available);
}