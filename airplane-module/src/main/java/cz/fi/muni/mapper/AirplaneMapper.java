package cz.fi.muni.mapper;

import cz.fi.muni.api.requestDto.AirplaneRequestDto;
import cz.fi.muni.api.responseDto.AirplaneResponseDto;
import cz.fi.muni.data.model.Airplane;
import cz.fi.muni.data.model.AirplaneType;
import cz.fi.muni.data.repository.AirplaneTypeRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public abstract class AirplaneMapper {

    @Autowired
    protected AirplaneTypeRepository airplaneTypeRepository;

    @Mapping(target = "type", source = "type", qualifiedByName = "stringToType")
    public abstract Airplane toEntity(AirplaneRequestDto requestDto);

    @Mapping(target = "type", source = "type", qualifiedByName = "typeToString")
    public abstract AirplaneResponseDto toResponseDto(Airplane airplane);

    public abstract List<AirplaneResponseDto> toResponseDtoList(List<Airplane> airplanes);

    @Named("stringToType")
    protected AirplaneType stringToType(String typeName) {
        return airplaneTypeRepository.findByName(typeName)
                .orElseThrow(() -> new IllegalArgumentException("Invalid airplane type: " + typeName));
    }

    @Named("typeToString")
    protected String typeToString(AirplaneType airplaneType) {
        return airplaneType.getName();
    }
}