package cz.fi.muni.service;

import cz.fi.muni.data.model.Airplane;

import java.util.List;

public interface AirplaneService {

    Airplane create(Airplane airplane);

    Airplane getById(Integer id);

    List<Airplane> getAll();

    Airplane update(Integer id, Airplane airplane);

    void delete(Integer id);

    void changeAvailability(Integer id, boolean available);
}