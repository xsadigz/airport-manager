package cz.fi.muni.service;

import cz.fi.muni.data.model.Airplane;
import cz.fi.muni.data.repository.AirplaneRepository;
import cz.fi.muni.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirplaneServiceImpl implements AirplaneService {

    private final AirplaneRepository airplaneRepository;

    @Autowired
    public AirplaneServiceImpl(AirplaneRepository airplaneRepository) {
        this.airplaneRepository = airplaneRepository;
    }

    @Override
    public Airplane create(Airplane airplane) {
        return airplaneRepository.save(airplane);
    }

    @Override
    public Airplane getById(Integer id) {
        return airplaneRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Airplane.class, id));
    }

    @Override
    public List<Airplane> getAll() {
        return airplaneRepository.findAll();
    }

    @Override
    public Airplane update(Integer id, Airplane airplane) {
        Airplane existingAirplane = getById(id);
        existingAirplane.setName(airplane.getName());
        existingAirplane.setType(airplane.getType());
        existingAirplane.setCapacity(airplane.getCapacity());
        existingAirplane.setRange(airplane.getRange());
        existingAirplane.setMaintenanceSchedule(airplane.getMaintenanceSchedule());
        existingAirplane.setYearOfManufacture(airplane.getYearOfManufacture());
        return airplaneRepository.save(existingAirplane);
    }

    @Override
    public void delete(Integer id) {
        airplaneRepository.deleteById(id);
    }

    @Override
    public void changeAvailability(Integer id, boolean available) {
        Airplane airplane = getById(id);
        airplane.setIsAvailable(available);
        airplaneRepository.save(airplane);
    }
}