
package cz.fi.muni.facade;

import cz.fi.muni.mapper.AirplaneMapper;
import cz.fi.muni.service.AirplaneServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AirplaneFacadeTest {

    @Mock
    private AirplaneMapper airplaneMapper;

    @Mock
    private AirplaneServiceImpl airplaneServiceImpl;

    @InjectMocks
    private AirplaneFacade airplaneFacade;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

//    @Test
//    public void createAirplaneTest() {
//        AirplaneRequestDto airplaneRequestDto = new AirplaneRequestDto();
//        airplaneRequestDto.setName("Boeing 747");
//        airplaneRequestDto.setType(AirplaneType.BOEING_747);
//        airplaneRequestDto.setCapacity(416);
//        airplaneRequestDto.setRange(13445.0);
//        airplaneRequestDto.setMaintenanceSchedule(LocalDate.now());
//        airplaneRequestDto.setYearOfManufacture(1969);
//
//        Airplane airplane = new Airplane();
//        airplane.setName("Boeing 747");
//        airplane.setType(AirplaneType.BOEING_747);
//        airplane.setCapacity(416);
//        airplane.setRange(13445.0);
//        airplane.setMaintenanceSchedule(LocalDate.now());
//        airplane.setYearOfManufacture(1969);
//
//        when(airplaneMapper.toEntity(any(AirplaneRequestDto.class))).thenReturn(airplane);
//
//        airplaneFacade.create(airplaneRequestDto);
//
//        verify(airplaneService, times(1)).create(any(Airplane.class));
//    }
//
//
//    @Test
//    public void getByIdTest() {
//        when(airplaneService.getById(anyInt())).thenReturn(new Airplane());
//
//        airplaneFacade.getById(1);
//
//        verify(airplaneService, times(1)).getById(anyInt());
//    }
//
//    @Test
//    public void getAllTest() {
//        when(airplaneService.getAll()).thenReturn(Collections.emptyList());
//
//        airplaneFacade.getAll();
//
//        verify(airplaneService, times(1)).getAll();
//    }
//
//    @Test
//    public void updateTest() {
//        AirplaneRequestDto airplaneRequestDto = new AirplaneRequestDto();
//        airplaneRequestDto.setName("Boeing 747");
//        airplaneRequestDto.setType(AirplaneType.BOEING_747);
//        airplaneRequestDto.setCapacity(416);
//        airplaneRequestDto.setRange(13445.0);
//        airplaneRequestDto.setMaintenanceSchedule(LocalDate.now());
//        airplaneRequestDto.setYearOfManufacture(1969);
//
//        Airplane airplane = new Airplane();
//        airplane.setName("Boeing 747");
//        airplane.setType(AirplaneType.BOEING_747);
//        airplane.setCapacity(416);
//        airplane.setRange(13445.0);
//        airplane.setMaintenanceSchedule(LocalDate.now());
//        airplane.setYearOfManufacture(1969);
//
//        when(airplaneMapper.toEntity(any(AirplaneRequestDto.class))).thenReturn(airplane);
//        when(airplaneService.update(anyInt(), any(Airplane.class))).thenReturn(airplane);
//
//        airplaneFacade.update(1, airplaneRequestDto);
//
//        verify(airplaneService, times(1)).update(anyInt(), any(Airplane.class));
//    }
//
//    @Test
//    public void deleteTest() {
//        airplaneFacade.delete(1);
//
//        verify(airplaneService, times(1)).delete(anyInt());
//    }
}