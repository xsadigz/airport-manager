package cz.fi.muni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DestinationServiceApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(DestinationServiceApp.class, args);
    }
}
