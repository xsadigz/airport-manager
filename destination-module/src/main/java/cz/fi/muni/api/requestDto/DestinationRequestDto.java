package cz.fi.muni.api.requestDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DestinationRequestDto {

    String name;
    Integer addressId;
    String countryCode;
    String airportSize;
    Integer trafficVolume;
    Integer runways;

}
