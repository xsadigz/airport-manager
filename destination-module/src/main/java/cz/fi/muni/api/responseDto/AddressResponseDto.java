package cz.fi.muni.api.responseDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressResponseDto {

    Integer id;
    String street;
    CityResponseDto city;
}
