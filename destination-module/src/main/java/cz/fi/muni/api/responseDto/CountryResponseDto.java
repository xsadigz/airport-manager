package cz.fi.muni.api.responseDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CountryResponseDto {

    Integer id;
    String name;
    String code;
}
