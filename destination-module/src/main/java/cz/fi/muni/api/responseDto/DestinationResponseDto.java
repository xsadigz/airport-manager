package cz.fi.muni.api.responseDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DestinationResponseDto {

    Integer id;
    String name;
    AddressResponseDto address;
    String country_code;
    String airport_size;
    Integer traffic_volume;
    Integer runways;

}
