package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.AddressRequestDto;
import cz.fi.muni.api.responseDto.AddressResponseDto;
import cz.fi.muni.facade.IAddressFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/addresses")
public class AddressController {

    private static final Logger log = LoggerFactory.getLogger(AddressController.class);

    private final IAddressFacade addressFacade;

    @Autowired
    public AddressController(IAddressFacade addressFacade) {
        this.addressFacade = addressFacade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addAddress(@RequestBody AddressRequestDto requestDto) {
        addressFacade.create(requestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAddress(@PathVariable("id") Integer id) {
        if (id == null) {
            log.info("Provided address identifier is null.");
        }
        addressFacade.delete(id);
    }

    @GetMapping
    public ResponseEntity<List<AddressResponseDto>> getAllAddresses() {
        log.info("getAllAddresses");
        return new ResponseEntity<>(addressFacade.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AddressResponseDto> getAddress(@PathVariable("id") Integer id) {
        if (id == null) {
            log.info("Provided address identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(addressFacade.getById(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> editAddress(@PathVariable Integer id, @RequestBody AddressRequestDto requestDto) {
        addressFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}