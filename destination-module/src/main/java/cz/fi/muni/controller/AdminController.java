package cz.fi.muni.controller;

import cz.fi.muni.facade.IAddressFacade;
import cz.fi.muni.facade.ICityFacade;
import cz.fi.muni.facade.ICountryFacade;
import cz.fi.muni.facade.IDestinationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    private final IAddressFacade addressFacade;
    private final ICityFacade cityFacade;
    private final ICountryFacade countryFacade;
    private final IDestinationFacade destinationFacade;


    @Autowired
    public AdminController(IAddressFacade addressFacade, ICityFacade cityFacade, ICountryFacade countryFacade, IDestinationFacade destinationFacade) {
        this.addressFacade = addressFacade;
        this.cityFacade = cityFacade;
        this.countryFacade = countryFacade;
        this.destinationFacade = destinationFacade;
    }

    @DeleteMapping("/clearDatabase")
    public void clearDatabase() {
        addressFacade.deleteAll();
        cityFacade.deleteAll();
        countryFacade.deleteAll();
        destinationFacade.deleteAll();
    }
}