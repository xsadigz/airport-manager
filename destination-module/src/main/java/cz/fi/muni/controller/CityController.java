package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.CityRequestDto;
import cz.fi.muni.api.responseDto.CityResponseDto;
import cz.fi.muni.facade.ICityFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/cities")
public class CityController {

    private static final Logger log = LoggerFactory.getLogger(CityController.class);

    private final ICityFacade cityFacade;

    @Autowired
    public CityController(ICityFacade cityFacade) {
        this.cityFacade = cityFacade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addCity(@RequestBody CityRequestDto requestDto) {
        cityFacade.create(requestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCity(@PathVariable("id") Integer id) {
        if (id == null) {
            log.info("Provided city identifier is null.");
        }
        cityFacade.delete(id);
    }


    @GetMapping
    public ResponseEntity<List<CityResponseDto>> getAllCities() {
        log.info("getAllCities");
        return new ResponseEntity<>(cityFacade.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CityResponseDto> getCity(@PathVariable("id") Integer id) {
        if (id == null) {
            log.info("Provided city identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cityFacade.getById(id), HttpStatus.OK);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Void> editCity(@PathVariable Integer id, @RequestBody CityRequestDto requestDto) {
        cityFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/byCountry/{id}")
    public ResponseEntity<List<CityResponseDto>> getCitiesByCountry(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(cityFacade.findAllByCountry(id), HttpStatus.OK);
    }

}
