package cz.fi.muni.controller;


import cz.fi.muni.api.requestDto.CountryRequestDto;
import cz.fi.muni.api.responseDto.CountryResponseDto;
import cz.fi.muni.facade.ICountryFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryController {

    private static final Logger log = LoggerFactory.getLogger(CountryController.class);

    private final ICountryFacade countryFacade;

    @Autowired
    public CountryController(ICountryFacade countryFacade) {
        this.countryFacade = countryFacade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addCountry(@RequestBody CountryRequestDto requestDto) {
        countryFacade.create(requestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCountry(@PathVariable("id") Integer id) {
        if (id == null) {
            log.info("Provided country identifier is null.");
        }
        countryFacade.delete(id);
    }


    @GetMapping
    public ResponseEntity<List<CountryResponseDto>> getAllCountries() {
        return new ResponseEntity<>(countryFacade.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CountryResponseDto> getCountry(@PathVariable("id") Integer id) {
        if (id == null) {
            log.info("Provided country identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(countryFacade.getById(id), HttpStatus.OK);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Void> editCountry(@PathVariable Integer id, @RequestBody CountryRequestDto requestDto) {
        countryFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
