package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.DestinationRequestDto;
import cz.fi.muni.api.responseDto.DestinationResponseDto;
import cz.fi.muni.facade.IDestinationFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/destinations")
public class DestinationController {

    private static final Logger log = LoggerFactory.getLogger(DestinationController.class);

    private final IDestinationFacade destinationFacade;

    @Autowired
    public DestinationController(IDestinationFacade destinationFacade) {
        this.destinationFacade = destinationFacade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addDestination(@RequestBody DestinationRequestDto requestDto) {
        destinationFacade.create(requestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDestination(@PathVariable("id") Integer id) {
        if (id == null) {
            log.info("Provided destination identifier is null.");
        }
        destinationFacade.delete(id);
    }


    @GetMapping
    public ResponseEntity<List<DestinationResponseDto>> getAllDestinations() {
        log.info("getAllDestinations");
        return new ResponseEntity<>(destinationFacade.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DestinationResponseDto> getDestination(@PathVariable("id") Integer id) {
        if (id == null) {
            log.info("Provided destination identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(destinationFacade.getById(id), HttpStatus.OK);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Void> editDestination(@PathVariable Integer id, @RequestBody DestinationRequestDto requestDto) {
        destinationFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
