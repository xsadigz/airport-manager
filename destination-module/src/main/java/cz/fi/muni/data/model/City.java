package cz.fi.muni.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class City implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @NotBlank(message = "Name cannot be blank")
    @Size(max = 100, message = "Name must be less than 100 characters")
    String name;

    @NotBlank(message = "Code cannot be blank")
    @Size(max = 4, message = "Code must be less than 4 characters")
    String code;

    @ManyToOne
    Country country;

    public Country getCountry(){
        if(Objects.isNull(country))
            return new Country();
        return country;
    }
}