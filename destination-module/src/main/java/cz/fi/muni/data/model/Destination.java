package cz.fi.muni.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Destination implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @NotBlank(message = "Name cannot be blank")
    String name;

    @NotNull(message = "Address cannot be null")
    @ManyToOne
    Address address;

    @NotBlank(message = "Airport size cannot be blank")
    String airportSize;

    @NotNull(message = "Traffic volume cannot be null")
    @Min(value = 1, message = "Traffic volume must be greater than 0")
    Integer trafficVolume;

    @NotNull(message = "Number of runways cannot be null")
    @Min(value = 1, message = "Number of runways must be greater than 0")
    Integer runways;

}