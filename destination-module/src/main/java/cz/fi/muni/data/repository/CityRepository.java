package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.City;
import cz.fi.muni.data.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> {
    List<City> findAllByCountry_Id(Integer countryId);
}
