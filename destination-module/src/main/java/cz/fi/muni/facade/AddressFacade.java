package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.AddressRequestDto;
import cz.fi.muni.api.responseDto.AddressResponseDto;
import cz.fi.muni.data.model.Address;
import cz.fi.muni.mapper.AddressMapper;
import cz.fi.muni.service.AddressService;
import cz.fi.muni.service.CityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressFacade implements IAddressFacade {

    private final AddressMapper addressMapper;
    private final AddressService addressService;
    private final CityService cityService;

    @Override
    public void create(AddressRequestDto requestDto) {
        Address address = addressMapper.toEntity(requestDto);
        address.setCity(cityService.getById(requestDto.getCityId()));
        addressService.create(address);
    }

    @Override
    @Transactional
    public AddressResponseDto getById(Integer id) {
        return addressMapper.toResponseDto(addressService.getById(id));
    }

    @Override
    @Transactional
    public List<AddressResponseDto> getAll() {
        return addressMapper.toResponseDtoList(addressService.getAll());
    }

    @Override
    public void update(Integer id, AddressRequestDto requestDto) {
        Address address = addressMapper.toEntity(requestDto);
        address.setCity(cityService.getById(requestDto.getCityId()));
        addressService.update(id, address);
    }

    @Override
    public void delete(Integer id) {
        addressService.delete(id);
    }

    @Override
    public void deleteAll() {
        addressService.deleteAll();
    }

}
