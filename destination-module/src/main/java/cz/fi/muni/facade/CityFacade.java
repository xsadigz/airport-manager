package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.CityRequestDto;
import cz.fi.muni.api.responseDto.CityResponseDto;
import cz.fi.muni.data.model.City;
import cz.fi.muni.mapper.CityMapper;
import cz.fi.muni.service.CityService;
import cz.fi.muni.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CityFacade implements ICityFacade{

    private final CityMapper cityMapper;
    private final CityService cityService;
    private final CountryService countryService;

    @Override
    public void create(CityRequestDto requestDto) {
        City city = cityMapper.toEntity(requestDto);
        Integer countryId = requestDto.getCountryId();
        city.setCountry(countryService.getById(countryId));
        cityService.create(city);
    }

    @Override
    @Transactional
    public CityResponseDto getById(Integer id) {
        return cityMapper.toResponseDto(cityService.getById(id));
    }

    @Override
    @Transactional
    public List<CityResponseDto> getAll() {
        return cityMapper.toResponseDtoList(cityService.getAll());
    }

    @Override
    public void update(Integer id, CityRequestDto requestDto) {
        City city = cityMapper.toEntity(requestDto);
        city.setCountry(countryService.getById(requestDto.getCountryId()));
        cityService.update(id, city);
    }

    @Override
    public void delete(Integer id) {
        cityService.delete(id);
    }

    @Override
    public List<CityResponseDto> findAllByCountry(Integer countryId) {
        return cityMapper.toResponseDtoList(cityService.findAllByCountry(countryId));
    }

    @Override
    public void deleteAll() {
        cityService.deleteAll();
    }

}
