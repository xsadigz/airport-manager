package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.CountryRequestDto;
import cz.fi.muni.api.responseDto.CountryResponseDto;
import cz.fi.muni.data.model.Country;
import cz.fi.muni.mapper.CountryMapper;
import cz.fi.muni.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CountryFacade implements ICountryFacade{

    private final CountryMapper countryMapper;
    private final CountryService countryService;

    @Override
    public void create(CountryRequestDto requestDto) {
        countryService.create(countryMapper.toEntity(requestDto));
    }

    @Override
    @Transactional
    public CountryResponseDto getById(Integer id) {
        return countryMapper.toResponseDto(countryService.getById(id));
    }

    @Override
    @Transactional
    public List<CountryResponseDto> getAll() {
        return countryMapper.toResponseDtoList(countryService.getAll());
    }

    @Override
    public void update(Integer id, CountryRequestDto requestDto) {
        countryService.update(id, countryMapper.toEntity(requestDto));
    }

    @Override
    public void delete(Integer id) {
        countryService.delete(id);
    }

    @Override
    public void deleteAll() {
        countryService.deleteAll();
    }
}
