package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.DestinationRequestDto;
import cz.fi.muni.api.responseDto.DestinationResponseDto;
import cz.fi.muni.data.model.Destination;
import cz.fi.muni.mapper.DestinationMapper;
import cz.fi.muni.service.DestinationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@RequiredArgsConstructor
public class DestinationFacade implements IDestinationFacade{

    private final DestinationMapper destinationMapper;
    private final DestinationService destinationService;

    @Override
    public void create(DestinationRequestDto requestDto) {
        Destination destination = destinationMapper.toEntity(requestDto);
        destinationService.create(destination);

    }

    @Override
    @Transactional
    public DestinationResponseDto getById(Integer id) {
        return destinationMapper.toResponseDto(destinationService.getById(id));
    }

    @Override
    @Transactional
    public List<DestinationResponseDto> getAll() {
        return destinationMapper.toResponseDtoList(destinationService.getAll());
    }

    @Override
    public void update(Integer id, DestinationRequestDto requestDto) {
        Destination destination = destinationMapper.toEntity(requestDto);
        destinationService.update(id, destination);
    }

    @Override
    public void delete(Integer id) {
        destinationService.delete(id);
    }

    @Override
    public void deleteAll() {
        destinationService.deleteAll();
    }
}
