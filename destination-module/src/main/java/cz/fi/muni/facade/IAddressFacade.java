package cz.fi.muni.facade;


import cz.fi.muni.api.requestDto.AddressRequestDto;
import cz.fi.muni.api.responseDto.AddressResponseDto;

import java.util.List;

public interface IAddressFacade {

    void create(AddressRequestDto requestDto);

    AddressResponseDto getById(Integer id);

    List<AddressResponseDto> getAll();

    void update(Integer id, AddressRequestDto requestDto);

    void delete(Integer id);

    void deleteAll();

}
