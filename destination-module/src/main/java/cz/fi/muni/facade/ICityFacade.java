package cz.fi.muni.facade;


import cz.fi.muni.api.requestDto.CityRequestDto;
import cz.fi.muni.api.responseDto.CityResponseDto;
import cz.fi.muni.data.model.City;

import java.util.List;

public interface ICityFacade {

    void create(CityRequestDto requestDto);

    CityResponseDto getById(Integer id);

    List<CityResponseDto> getAll();

    void update(Integer id, CityRequestDto requestDto);

    void delete(Integer id);

    List<CityResponseDto> findAllByCountry(Integer countryId);

    void deleteAll();

}
