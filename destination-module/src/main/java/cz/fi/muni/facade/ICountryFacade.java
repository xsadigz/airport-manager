package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.CountryRequestDto;
import cz.fi.muni.api.responseDto.CountryResponseDto;

import java.util.List;

public interface ICountryFacade {

    void create(CountryRequestDto requestDto);

    CountryResponseDto getById(Integer id);

    List<CountryResponseDto> getAll();

    void update(Integer id, CountryRequestDto requestDto);

    void delete(Integer id);

    void deleteAll();

}
