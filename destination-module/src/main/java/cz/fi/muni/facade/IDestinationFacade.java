package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.DestinationRequestDto;
import cz.fi.muni.api.responseDto.DestinationResponseDto;

import java.util.List;

public interface IDestinationFacade {

    void create(DestinationRequestDto requestDto);

    DestinationResponseDto getById(Integer id);

    List<DestinationResponseDto> getAll();

    void update(Integer id, DestinationRequestDto requestDto);

    void delete(Integer id);

    void deleteAll();

}
