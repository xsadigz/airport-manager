package cz.fi.muni.mapper;

import cz.fi.muni.api.requestDto.AddressRequestDto;
import cz.fi.muni.api.responseDto.AddressResponseDto;
import cz.fi.muni.data.model.Address;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface AddressMapper {

    Address toEntity(AddressRequestDto requestDto);

    AddressResponseDto toResponseDto(Address address);

    List<AddressResponseDto> toResponseDtoList(List<Address> addresses);

}
