package cz.fi.muni.mapper;

import cz.fi.muni.api.requestDto.CityRequestDto;
import cz.fi.muni.api.responseDto.CityResponseDto;
import cz.fi.muni.data.model.City;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface CityMapper {

    City toEntity(CityRequestDto requestDto);

    CityResponseDto toResponseDto(City city);

    List<CityResponseDto> toResponseDtoList(List<City> cities);

}
