package cz.fi.muni.mapper;


import cz.fi.muni.api.requestDto.DestinationRequestDto;
import cz.fi.muni.api.responseDto.DestinationResponseDto;
import cz.fi.muni.data.model.Destination;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface DestinationMapper {

    Destination toEntity(DestinationRequestDto requestDto);

    DestinationResponseDto toResponseDto(Destination destination);

    List<DestinationResponseDto> toResponseDtoList(List<Destination> destinations);

}
