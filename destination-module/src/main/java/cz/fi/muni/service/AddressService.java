package cz.fi.muni.service;

import cz.fi.muni.data.model.Address;
import cz.fi.muni.data.repository.AddressRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressService implements IAddressService{

    private final AddressRepository addressRepository;

    @Override
    public Address create(Address address) {
        return addressRepository.save(address);
    }

    @Override
    @Transactional
    public Address getById(Integer id) {
        return addressRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(Address.class, id));
    }

    @Override
    @Transactional
    public List<Address> getAll() {
        return addressRepository.findAll();
    }

    @Override
    public Address update(Integer id, Address address) {
        Address existingAddress = addressRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Address.class, id));
        existingAddress.setStreet(address.getStreet());
        return addressRepository.save(existingAddress);
    }

    @Override
    public void delete(Integer id) {
        addressRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        addressRepository.deleteAll();
    }

}
