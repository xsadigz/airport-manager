package cz.fi.muni.service;

import cz.fi.muni.data.model.City;
import cz.fi.muni.data.repository.CityRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CityService implements ICityService{

    private final CityRepository cityRepository;

    @Override
    public City create(City city) {
        return cityRepository.save(city);
    }

    @Override
    @Transactional
    public City getById(Integer id) {
        return cityRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(City.class, id));
    }

    @Override
    @Transactional
    public List<City> getAll() {
        return cityRepository.findAll();
    }

    @Override
    public City update(Integer id, City city) {
        City existingCity = cityRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(City.class, id));
        existingCity.setName(city.getName());
        return cityRepository.save(existingCity);
    }

    @Override
    public void delete(Integer id) {
        cityRepository.deleteById(id);
    }

    @Override
    public List<City> findAllByCountry(Integer countryId) {
        return cityRepository.findAllByCountry_Id(countryId);
    }

    @Override
    public void deleteAll() {
        cityRepository.deleteAll();
    }
}


