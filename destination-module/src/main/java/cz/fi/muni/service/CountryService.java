package cz.fi.muni.service;

import cz.fi.muni.data.model.Country;
import cz.fi.muni.data.repository.CountryRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CountryService implements ICountryService {

    private final CountryRepository countryRepository;

    @Override
    public Country create(Country country) {
        return countryRepository.save(country);
    }

    @Override
    @Transactional
    public Country getById(Integer id) {
        return countryRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(Country.class, id));
    }

    @Override
    @Transactional
    public List<Country> getAll() {
        return countryRepository.findAll();
    }

    @Override
    public Country update(Integer id, Country country) {
        Country existingCountry = countryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Country.class, id));
        existingCountry.setName(country.getName());
        existingCountry.setCode(country.getCode());
        return countryRepository.save(existingCountry);
    }

    @Override
    public void delete(Integer id) {
        countryRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        countryRepository.deleteAll();
    }
}
