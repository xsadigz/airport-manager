package cz.fi.muni.service;

import cz.fi.muni.data.model.Destination;
import cz.fi.muni.data.repository.DestinationRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@RequiredArgsConstructor
public class DestinationService implements IDestinationService{

    private final DestinationRepository destinationRepository;

    @Override
    public Destination create(Destination destination) {
        return destinationRepository.save(destination);
    }

    @Override
    @Transactional
    public Destination getById(Integer id) {
        return destinationRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Destination.class, id));
    }

    @Override
    @Transactional
    public List<Destination> getAll() {
        return destinationRepository.findAll();
    }

    @Override
    public Destination update(Integer id, Destination destination) {
        Destination existingDestination = destinationRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Destination.class, id));
        existingDestination.setName(destination.getName());
        existingDestination.setAddress(destination.getAddress());
        existingDestination.setAirportSize(destination.getAirportSize());
        existingDestination.setTrafficVolume(destination.getTrafficVolume());
        existingDestination.setRunways(destination.getRunways());
        return destinationRepository.save(existingDestination);
    }

    @Override
    public void delete(Integer id) {
        destinationRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        destinationRepository.deleteAll();
    }

}
