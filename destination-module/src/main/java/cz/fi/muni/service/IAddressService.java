package cz.fi.muni.service;

import cz.fi.muni.data.model.Address;

import java.util.List;

public interface IAddressService {

    Address create(Address address);

    Address getById(Integer id);

    List<Address> getAll();

    Address update(Integer id, Address address);

    void delete(Integer id);

    void deleteAll();
}
