package cz.fi.muni.service;

import cz.fi.muni.data.model.City;

import java.util.List;

public interface ICityService {

    City create(City city);

    City getById(Integer id);

    List<City> getAll();

    City update(Integer id, City city);

    void delete(Integer id);

    List<City> findAllByCountry(Integer countryId);

    void deleteAll();

}
