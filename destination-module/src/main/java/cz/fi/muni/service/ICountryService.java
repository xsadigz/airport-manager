package cz.fi.muni.service;

import cz.fi.muni.data.model.Country;

import java.util.List;

public interface ICountryService {

    Country create(Country country);

    Country getById(Integer id);

    List<Country> getAll();

    Country update(Integer id, Country country);

    void delete(Integer id);

    void deleteAll();

}
