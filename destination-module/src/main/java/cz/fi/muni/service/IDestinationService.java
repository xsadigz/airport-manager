package cz.fi.muni.service;

import cz.fi.muni.data.model.Destination;

import java.util.List;

public interface IDestinationService {

    Destination create(Destination destination);

    Destination getById(Integer id);

    List<Destination> getAll();

    Destination update(Integer id, Destination destination);

    void delete(Integer id);
    void deleteAll();

}
