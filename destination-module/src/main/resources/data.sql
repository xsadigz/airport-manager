INSERT INTO country (id, name, code) VALUES (4, 'United States', 'US');
INSERT INTO country (id, name, code) VALUES (2, 'Canada', 'CA');
INSERT INTO country (id, name, code) VALUES (3, 'Mexico', 'MX');

INSERT INTO city (id, name, country_id) VALUES (1, 'New York', 1);
INSERT INTO city (id, name, country_id) VALUES (2, 'Los Angeles', 1);
INSERT INTO city (id, name, country_id) VALUES (3, 'Toronto', 2);
INSERT INTO city (id, name, country_id) VALUES (4, 'Vancouver', 2);
INSERT INTO city (id, name, country_id) VALUES (5, 'Mexico City', 3);
INSERT INTO city (id, name, country_id) VALUES (6, 'Guadalajara', 3);

INSERT INTO address (id, street, city_id) VALUES (1, '123 Main St', 1);
INSERT INTO address (id, street, city_id) VALUES (2, '456 Maple Ave', 2);
INSERT INTO address (id, street, city_id) VALUES (3, '789 Oak Dr', 3);
INSERT INTO address (id, street, city_id) VALUES (4, '321 Pine Ln', 4);
INSERT INTO address (id, street, city_id) VALUES (5, '654 Elm St', 5);
INSERT INTO address (id, street, city_id) VALUES (6, '987 Cedar Ave', 6);

INSERT INTO destination (id, name, address_id, airport_size, traffic_volume, runways) VALUES (1, 'Statue of Liberty', 1, 'Small', 10000, 1);
INSERT INTO destination (id, name, address_id, airport_size, traffic_volume, runways) VALUES (2, 'Hollywood Sign', 2, 'Medium', 20000, 2);
INSERT INTO destination (id, name, address_id, airport_size, traffic_volume, runways) VALUES (3, 'CN Tower', 3, 'Large', 30000, 3);
INSERT INTO destination (id, name, address_id, airport_size, traffic_volume, runways) VALUES (4, 'Stanley Park', 4, 'Small', 40000, 1);
INSERT INTO destination (id, name, address_id, airport_size, traffic_volume, runways) VALUES (5, 'Zocalo', 5, 'Medium', 50000, 2);
INSERT INTO destination (id, name, address_id, airport_size, traffic_volume, runways) VALUES (6, 'Guadalajara Cathedral', 6, 'Large', 60000, 3);
