package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.AddressRequestDto;
import cz.fi.muni.api.responseDto.AddressResponseDto;
import cz.fi.muni.facade.IAddressFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AddressControllerTest {

    @Mock
    private IAddressFacade addressFacade;

    @InjectMocks
    private AddressController addressController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void addAddressTest() {
        AddressRequestDto addressRequestDto = new AddressRequestDto();
        addressRequestDto.setStreet("Street Name");
        addressRequestDto.setCityId(1);

        addressController.addAddress(addressRequestDto);

        verify(addressFacade, times(1)).create(any(AddressRequestDto.class));
    }

    @Test
    public void getAddressTest() {
        when(addressFacade.getById(anyInt())).thenReturn(new AddressResponseDto());

        addressController.getAddress(1);

        verify(addressFacade, times(1)).getById(anyInt());
    }

    @Test
    public void getAllAddressesTest() {
        when(addressFacade.getAll()).thenReturn(Collections.emptyList());

        addressController.getAllAddresses();

        verify(addressFacade, times(1)).getAll();
    }

    @Test
    public void editAddressTest() {
        AddressRequestDto addressRequestDto = new AddressRequestDto();
        addressRequestDto.setStreet("Street Name");
        addressRequestDto.setCityId(1);

        addressController.editAddress(1, addressRequestDto);

        verify(addressFacade, times(1)).update(anyInt(), any(AddressRequestDto.class));
    }

    @Test
    public void deleteAddressTest() {
        addressController.deleteAddress(1);

        verify(addressFacade, times(1)).delete(anyInt());
    }
}