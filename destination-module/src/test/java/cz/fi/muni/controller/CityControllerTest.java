package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.CityRequestDto;
import cz.fi.muni.api.responseDto.CityResponseDto;
import cz.fi.muni.facade.ICityFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CityControllerTest {

    @Mock
    private ICityFacade cityFacade;

    @InjectMocks
    private CityController cityController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void addCityTest() {
        CityRequestDto cityRequestDto = new CityRequestDto();
        cityRequestDto.setName("City Name");
        cityRequestDto.setCountryId(1);

        cityController.addCity(cityRequestDto);

        verify(cityFacade, times(1)).create(any(CityRequestDto.class));
    }

    @Test
    public void getCityTest() {
        when(cityFacade.getById(anyInt())).thenReturn(new CityResponseDto());

        cityController.getCity(1);

        verify(cityFacade, times(1)).getById(anyInt());
    }

    @Test
    public void getAllCitiesTest() {
        when(cityFacade.getAll()).thenReturn(Collections.emptyList());

        cityController.getAllCities();

        verify(cityFacade, times(1)).getAll();
    }

    @Test
    public void editCityTest() {
        CityRequestDto cityRequestDto = new CityRequestDto();
        cityRequestDto.setName("City Name");
        cityRequestDto.setCountryId(1);

        cityController.editCity(1, cityRequestDto);

        verify(cityFacade, times(1)).update(anyInt(), any(CityRequestDto.class));
    }

    @Test
    public void deleteCityTest() {
        cityController.deleteCity(1);

        verify(cityFacade, times(1)).delete(anyInt());
    }
}