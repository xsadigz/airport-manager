package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.CountryRequestDto;
import cz.fi.muni.api.responseDto.CountryResponseDto;
import cz.fi.muni.facade.ICountryFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CountryControllerTest {

    @Mock
    private ICountryFacade countryFacade;

    @InjectMocks
    private CountryController countryController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void addCountryTest() {
        CountryRequestDto countryRequestDto = new CountryRequestDto();
        countryRequestDto.setName("Country Name");
        countryRequestDto.setCode("Country Code");

        countryController.addCountry(countryRequestDto);

        verify(countryFacade, times(1)).create(any(CountryRequestDto.class));
    }

    @Test
    public void getCountryTest() {
        when(countryFacade.getById(anyInt())).thenReturn(new CountryResponseDto());

        countryController.getCountry(1);

        verify(countryFacade, times(1)).getById(anyInt());
    }

    @Test
    public void getAllCountriesTest() {
        when(countryFacade.getAll()).thenReturn(Collections.emptyList());

        countryController.getAllCountries();

        verify(countryFacade, times(1)).getAll();
    }

    @Test
    public void editCountryTest() {
        CountryRequestDto countryRequestDto = new CountryRequestDto();
        countryRequestDto.setName("Country Name");
        countryRequestDto.setCode("Country Code");

        countryController.editCountry(1, countryRequestDto);

        verify(countryFacade, times(1)).update(anyInt(), any(CountryRequestDto.class));
    }

    @Test
    public void deleteCountryTest() {
        countryController.deleteCountry(1);

        verify(countryFacade, times(1)).delete(anyInt());
    }
}