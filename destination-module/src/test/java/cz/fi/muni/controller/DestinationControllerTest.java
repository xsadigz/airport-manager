package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.DestinationRequestDto;
import cz.fi.muni.api.responseDto.DestinationResponseDto;
import cz.fi.muni.facade.IDestinationFacade;
import cz.fi.muni.mapper.DestinationMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DestinationControllerTest {

    @Mock
    private IDestinationFacade destinationFacade;

    @InjectMocks
    private DestinationController destinationController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void addDestinationTest() {
        DestinationRequestDto destinationRequestDto = new DestinationRequestDto();
        destinationRequestDto.setName("Airport Name");
        destinationRequestDto.setAddressId(1);
        destinationRequestDto.setCountryCode("Country Code");
        destinationRequestDto.setAirportSize("Airport Size");
        destinationRequestDto.setTrafficVolume(1000);
        destinationRequestDto.setRunways(2);

        destinationController.addDestination(destinationRequestDto);

        verify(destinationFacade, times(1)).create(any(DestinationRequestDto.class));
    }

    @Test
    public void getDestinationTest() {
        when(destinationFacade.getById(anyInt())).thenReturn(new DestinationResponseDto());

        destinationController.getDestination(1);

       // verify(destinationFacade, times(1)).getById(anyInt());
    }

    @Test
    public void getAllDestinationsTest() {
        when(destinationFacade.getAll()).thenReturn(Collections.emptyList());

        destinationController.getAllDestinations();

        verify(destinationFacade, times(1)).getAll();
    }

    @Test
    public void editDestinationTest() {
        DestinationRequestDto destinationRequestDto = new DestinationRequestDto();
        destinationRequestDto.setName("Airport Name");
        destinationRequestDto.setAddressId(1);
        destinationRequestDto.setCountryCode("Country Code");
        destinationRequestDto.setAirportSize("Airport Size");
        destinationRequestDto.setTrafficVolume(1000);
        destinationRequestDto.setRunways(2);

        destinationController.editDestination(1, destinationRequestDto);

        verify(destinationFacade, times(1)).update(anyInt(), any(DestinationRequestDto.class));
    }

    @Test
    public void deleteDestinationTest() {
        destinationController.deleteDestination(1);

        verify(destinationFacade, times(1)).delete(anyInt());
    }
}