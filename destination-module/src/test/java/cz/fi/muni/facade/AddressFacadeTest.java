package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.AddressRequestDto;
import cz.fi.muni.api.responseDto.AddressResponseDto;
import cz.fi.muni.data.model.Address;
import cz.fi.muni.data.model.City;
import cz.fi.muni.mapper.AddressMapper;
import cz.fi.muni.service.AddressService;
import cz.fi.muni.service.CityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AddressFacadeTest {

    @Mock
    private AddressMapper addressMapper;

    @Mock
    private AddressService addressService;

    @Mock
    private CityService cityService;

    @InjectMocks
    private AddressFacade addressFacade;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        addressFacade = new AddressFacade(addressMapper, addressService, cityService);

    }

    @Test
    public void createAddressTest() {
        AddressRequestDto addressRequestDto = new AddressRequestDto();
        addressRequestDto.setCityId(1);
        addressRequestDto.setStreet("Street");

        Address address = new Address();
        City city = new City();
        address.setCity(city);
        address.setStreet("Street");

        when(addressMapper.toEntity(any(AddressRequestDto.class))).thenReturn(address);

        addressFacade.create(addressRequestDto);

        verify(addressService, times(1)).create(any(Address.class));
    }


    @Test
    public void getByIdTest() {
        when(addressService.getById(anyInt())).thenReturn(new Address());

        addressFacade.getById(1);

        verify(addressService, times(1)).getById(anyInt());
    }

    @Test
    public void getAllTest() {
        when(addressService.getAll()).thenReturn(Collections.emptyList());

        addressFacade.getAll();

        verify(addressService, times(1)).getAll();
    }

    @Test
    public void updateTest() {
        AddressRequestDto addressRequestDto = new AddressRequestDto();
        addressRequestDto.setStreet("Street Name");
        addressRequestDto.setCityId(1);

        Address address = new Address();
        address.setStreet("Street Name");

        when(addressMapper.toEntity(any(AddressRequestDto.class))).thenReturn(address);
        when(addressService.update(anyInt(), any(Address.class))).thenReturn(address);

        addressFacade.update(1, addressRequestDto);

        verify(addressService, times(1)).update(anyInt(), any(Address.class));
    }

    @Test
    public void deleteTest() {
        addressFacade.delete(1);

        verify(addressService, times(1)).delete(anyInt());
    }
}