package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.CityRequestDto;
import cz.fi.muni.data.model.City;
import cz.fi.muni.mapper.CityMapper;
import cz.fi.muni.service.CityService;
import cz.fi.muni.service.CountryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CityFacadeTest {

    @Mock
    private CityMapper cityMapper;

    @Mock
    private CityService cityService;

    @Mock
    private CountryService countryService;

    @InjectMocks
    private CityFacade cityFacade;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        cityFacade = new CityFacade(cityMapper, cityService, countryService);


    }

    @Test
    public void createCityTest() {
        CityRequestDto cityRequestDto = new CityRequestDto();
        cityRequestDto.setName("City Name");
        cityRequestDto.setCountryId(1);

        City city = new City();
        city.setName("City Name");

        when(cityMapper.toEntity(any(CityRequestDto.class))).thenReturn(city);

        cityFacade.create(cityRequestDto);

        verify(cityService, times(1)).create(any(City.class));
    }

    @Test
    public void getByIdTest() {
        when(cityService.getById(anyInt())).thenReturn(new City());

        cityFacade.getById(1);

        verify(cityService, times(1)).getById(anyInt());
    }

    @Test
    public void getAllTest() {
        when(cityService.getAll()).thenReturn(Collections.emptyList());

        cityFacade.getAll();

        verify(cityService, times(1)).getAll();
    }

    @Test
    public void updateTest() {
        CityRequestDto cityRequestDto = new CityRequestDto();
        cityRequestDto.setName("City Name");
        cityRequestDto.setCountryId(1);

        City city = new City();
        city.setName("City Name");

        when(cityMapper.toEntity(any(CityRequestDto.class))).thenReturn(city);
        when(cityService.update(anyInt(), any(City.class))).thenReturn(city);

        cityFacade.update(1, cityRequestDto);

        verify(cityService, times(1)).update(anyInt(), any(City.class));
    }

    @Test
    public void deleteTest() {
        cityFacade.delete(1);

        verify(cityService, times(1)).delete(anyInt());
    }
}