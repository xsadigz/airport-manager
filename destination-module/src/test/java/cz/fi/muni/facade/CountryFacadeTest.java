package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.CountryRequestDto;
import cz.fi.muni.data.model.Country;
import cz.fi.muni.mapper.CountryMapper;
import cz.fi.muni.service.CountryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CountryFacadeTest {

    @Mock
    private CountryMapper countryMapper;

    @Mock
    private CountryService countryService;

    @InjectMocks
    private CountryFacade countryFacade;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void createCountryTest() {
        CountryRequestDto countryRequestDto = new CountryRequestDto();
        countryRequestDto.setName("Country Name");
        countryRequestDto.setCode("Country Code");

        Country country = new Country();
        country.setName("Country Name");
        country.setCode("Country Code");

        when(countryMapper.toEntity(any(CountryRequestDto.class))).thenReturn(country);

        countryFacade.create(countryRequestDto);

        verify(countryService, times(1)).create(any(Country.class));
    }

    @Test
    public void getByIdTest() {
        when(countryService.getById(anyInt())).thenReturn(new Country());

        countryFacade.getById(1);

        verify(countryService, times(1)).getById(anyInt());
    }

    @Test
    public void getAllTest() {
        when(countryService.getAll()).thenReturn(Collections.emptyList());

        countryFacade.getAll();

        verify(countryService, times(1)).getAll();
    }

    @Test
    public void updateTest() {
        CountryRequestDto countryRequestDto = new CountryRequestDto();
        countryRequestDto.setName("Country Name");
        countryRequestDto.setCode("Country Code");

        Country country = new Country();
        country.setName("Country Name");
        country.setCode("Country Code");

        when(countryMapper.toEntity(any(CountryRequestDto.class))).thenReturn(country);
        when(countryService.update(anyInt(), any(Country.class))).thenReturn(country);

        countryFacade.update(1, countryRequestDto);

        verify(countryService, times(1)).update(anyInt(), any(Country.class));
    }

    @Test
    public void deleteTest() {
        countryFacade.delete(1);

        verify(countryService, times(1)).delete(anyInt());
    }
}