package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.DestinationRequestDto;
import cz.fi.muni.data.model.Address;
import cz.fi.muni.data.model.Destination;
import cz.fi.muni.mapper.DestinationMapper;
import cz.fi.muni.service.DestinationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DestinationFacadeTest {

    @Mock
    private DestinationMapper destinationMapper;

    @Mock
    private DestinationService destinationService;

    @InjectMocks
    private DestinationFacade destinationFacade;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void createDestinationTest() {
        DestinationRequestDto destinationRequestDto = new DestinationRequestDto();
        destinationRequestDto.setName("Airport Name");
        destinationRequestDto.setAddressId(1);
        destinationRequestDto.setCountryCode("Country Code");
        destinationRequestDto.setAirportSize("Airport Size");
        destinationRequestDto.setTrafficVolume(1000);
        destinationRequestDto.setRunways(2);

        Destination destination = new Destination();
        Address address = new Address();
        destination.setName("Airport Name");
        destination.setAddress(address);
        destination.setAirportSize("Airport Size");
        destination.setTrafficVolume(1000);
        destination.setRunways(2);

        when(destinationMapper.toEntity(any(DestinationRequestDto.class))).thenReturn(destination);

        destinationFacade.create(destinationRequestDto);

        verify(destinationService, times(1)).create(any(Destination.class));
    }

    @Test
    public void getByIdTest() {
        when(destinationService.getById(anyInt())).thenReturn(new Destination());

        destinationFacade.getById(1);

        verify(destinationService, times(1)).getById(anyInt());
    }

    @Test
    public void getAllTest() {
        when(destinationService.getAll()).thenReturn(Collections.emptyList());

        destinationFacade.getAll();

        verify(destinationService, times(1)).getAll();
    }

    @Test
    public void updateTest() {
        DestinationRequestDto destinationRequestDto = new DestinationRequestDto();
        destinationRequestDto.setName("Airport Name");
        destinationRequestDto.setAddressId(1);
        destinationRequestDto.setCountryCode("Country Code");
        destinationRequestDto.setAirportSize("Airport Size");
        destinationRequestDto.setTrafficVolume(1000);
        destinationRequestDto.setRunways(2);

        Destination destination = new Destination();
        Address address = new Address();
        destination.setName("Airport Name");
        destination.setAddress(address);
        destination.setAirportSize("Airport Size");
        destination.setTrafficVolume(1000);
        destination.setRunways(2);

        when(destinationMapper.toEntity(any(DestinationRequestDto.class))).thenReturn(destination);
        when(destinationService.update(anyInt(), any(Destination.class))).thenReturn(destination);

        destinationFacade.update(1, destinationRequestDto);

        verify(destinationService, times(1)).update(anyInt(), any(Destination.class));
    }

    @Test
    public void deleteTest() {
        destinationFacade.delete(1);

        verify(destinationService, times(1)).delete(anyInt());
    }
}