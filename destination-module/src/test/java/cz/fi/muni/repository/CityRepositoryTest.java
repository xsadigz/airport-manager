//package cz.fi.muni.repository;
//
//import cz.fi.muni.data.model.City;
//import cz.fi.muni.data.model.Country;
//import cz.fi.muni.data.repository.CityRepository;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
//
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//@DataJpaTest
//public class CityRepositoryTest {
//
//    @Autowired
//    private TestEntityManager entityManager;
//
//    @Autowired
//    private CityRepository cityRepository;
//
//
//
//    @Test
//    public void testFindAllByCountryId() {
//        List<City> cities = cityRepository.findAllByCountry_Id(1);
//
//        assertEquals(2, cities.size());
//        assertEquals("New York", cities.get(0).getName());
//        assertEquals("Los Angeles", cities.get(1).getName());
//    }
//
//}