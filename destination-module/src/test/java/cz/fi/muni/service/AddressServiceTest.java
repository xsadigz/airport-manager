package cz.fi.muni.service;

import cz.fi.muni.data.model.Address;
import cz.fi.muni.data.repository.AddressRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

public class AddressServiceTest {

    @InjectMocks
    private AddressService addressService;

    @Mock
    private AddressRepository addressRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateAddress() {
        Address address = new Address();
        address.setStreet("Test Street");

        when(addressRepository.save(address)).thenReturn(address);

        Address saved = addressService.create(address);

        assertEquals(address.getStreet(), saved.getStreet());
        verify(addressRepository, times(1)).save(address);
    }

    @Test
    public void testGetAddressById() {
        Address address = new Address();
        address.setId(1);
        address.setStreet("Test Street");

        when(addressRepository.findById(1)).thenReturn(Optional.of(address));

        Address found = addressService.getById(1);

        assertEquals(address.getStreet(), found.getStreet());
        verify(addressRepository, times(1)).findById(1);
    }

    @Test
    public void testGetAllAddresses() {
        Address address1 = new Address();
        address1.setStreet("Test Street 1");

        Address address2 = new Address();
        address2.setStreet("Test Street 2");

        when(addressRepository.findAll()).thenReturn(Arrays.asList(address1, address2));

        List<Address> addresses = addressService.getAll();

        assertEquals(2, addresses.size());
        verify(addressRepository, times(1)).findAll();
    }

    @Test
    public void testUpdateAddress() {
        Address address = new Address();
        address.setId(1);
        address.setStreet("Test Street");

        when(addressRepository.findById(1)).thenReturn(Optional.of(address));
        when(addressRepository.save(address)).thenReturn(address);

        Address updated = addressService.update(1, address);

        assertEquals(address.getStreet(), updated.getStreet());
        verify(addressRepository, times(1)).findById(1);
        verify(addressRepository, times(1)).save(address);
    }

    @Test
    public void testDeleteAddress() {
        Address address = new Address();
        address.setId(1);

        addressService.delete(1);

        verify(addressRepository, times(1)).deleteById(1);
    }

    @Test
    public void testDeleteAllAddresses() {
        addressService.deleteAll();

        verify(addressRepository, times(1)).deleteAll();
    }
}