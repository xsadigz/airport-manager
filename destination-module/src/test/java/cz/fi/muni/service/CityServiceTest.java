package cz.fi.muni.service;

import cz.fi.muni.data.model.City;
import cz.fi.muni.data.repository.CityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

public class CityServiceTest {

    @InjectMocks
    private CityService cityService;

    @Mock
    private CityRepository cityRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateCity() {
        City city = new City();
        city.setName("Test City");

        when(cityRepository.save(city)).thenReturn(city);

        City saved = cityService.create(city);

        assertEquals(city.getName(), saved.getName());
        verify(cityRepository, times(1)).save(city);
    }

    @Test
    public void testGetCityById() {
        City city = new City();
        city.setId(1);
        city.setName("Test City");

        when(cityRepository.findById(1)).thenReturn(Optional.of(city));

        City found = cityService.getById(1);

        assertEquals(city.getName(), found.getName());
        verify(cityRepository, times(1)).findById(1);
    }

    @Test
    public void testGetAllCities() {
        City city1 = new City();
        city1.setName("Test City 1");

        City city2 = new City();
        city2.setName("Test City 2");

        when(cityRepository.findAll()).thenReturn(Arrays.asList(city1, city2));

        List<City> cities = cityService.getAll();

        assertEquals(2, cities.size());
        verify(cityRepository, times(1)).findAll();
    }

    @Test
    public void testUpdateCity() {
        City city = new City();
        city.setId(1);
        city.setName("Test City");

        when(cityRepository.findById(1)).thenReturn(Optional.of(city));
        when(cityRepository.save(city)).thenReturn(city);

        City updated = cityService.update(1, city);

        assertEquals(city.getName(), updated.getName());
        verify(cityRepository, times(1)).findById(1);
        verify(cityRepository, times(1)).save(city);
    }

    @Test
    public void testDeleteCity() {
        City city = new City();
        city.setId(1);

        cityService.delete(1);

        verify(cityRepository, times(1)).deleteById(1);
    }

    @Test
    public void testFindAllByCountry() {
        City city1 = new City();
        city1.setName("Test City 1");

        City city2 = new City();
        city2.setName("Test City 2");

        when(cityRepository.findAllByCountry_Id(1)).thenReturn(Arrays.asList(city1, city2));

        List<City> cities = cityService.findAllByCountry(1);

        assertEquals(2, cities.size());
        verify(cityRepository, times(1)).findAllByCountry_Id(1);
    }

    @Test
    public void testDeleteAllCities() {
        cityService.deleteAll();

        verify(cityRepository, times(1)).deleteAll();
    }
}