package cz.fi.muni.service;

import cz.fi.muni.data.model.Country;
import cz.fi.muni.data.repository.CountryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

public class CountryServiceTest {

    @InjectMocks
    private CountryService countryService;

    @Mock
    private CountryRepository countryRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateCountry() {
        Country country = new Country();
        country.setName("Test Country");

        when(countryRepository.save(country)).thenReturn(country);

        Country saved = countryService.create(country);

        assertEquals(country.getName(), saved.getName());
        verify(countryRepository, times(1)).save(country);
    }

    @Test
    public void testGetCountryById() {
        Country country = new Country();
        country.setId(1);
        country.setName("Test Country");

        when(countryRepository.findById(1)).thenReturn(Optional.of(country));

        Country found = countryService.getById(1);

        assertEquals(country.getName(), found.getName());
        verify(countryRepository, times(1)).findById(1);
    }

    @Test
    public void testGetAllCountries() {
        Country country1 = new Country();
        country1.setName("Test Country 1");

        Country country2 = new Country();
        country2.setName("Test Country 2");

        when(countryRepository.findAll()).thenReturn(Arrays.asList(country1, country2));

        List<Country> countries = countryService.getAll();

        assertEquals(2, countries.size());
        verify(countryRepository, times(1)).findAll();
    }

    @Test
    public void testUpdateCountry() {
        Country country = new Country();
        country.setId(1);
        country.setName("Test Country");

        when(countryRepository.findById(1)).thenReturn(Optional.of(country));
        when(countryRepository.save(country)).thenReturn(country);

        Country updated = countryService.update(1, country);

        assertEquals(country.getName(), updated.getName());
        verify(countryRepository, times(1)).findById(1);
        verify(countryRepository, times(1)).save(country);
    }

    @Test
    public void testDeleteCountry() {
        Country country = new Country();
        country.setId(1);

        countryService.delete(1);

        verify(countryRepository, times(1)).deleteById(1);
    }

    @Test
    public void testDeleteAllCountries() {
        countryService.deleteAll();

        verify(countryRepository, times(1)).deleteAll();
    }
}