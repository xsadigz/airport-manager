package cz.fi.muni.service;

import cz.fi.muni.data.model.Destination;
import cz.fi.muni.data.repository.DestinationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

public class DestinationServiceTest {

    @InjectMocks
    private DestinationService destinationService;

    @Mock
    private DestinationRepository destinationRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateDestination() {
        Destination destination = new Destination();
        destination.setName("Test Destination");

        when(destinationRepository.save(destination)).thenReturn(destination);

        Destination saved = destinationService.create(destination);

        assertEquals(destination.getName(), saved.getName());
        verify(destinationRepository, times(1)).save(destination);
    }

    @Test
    public void testGetDestinationById() {
        Destination destination = new Destination();
        destination.setId(1);
        destination.setName("Test Destination");

        when(destinationRepository.findById(1)).thenReturn(Optional.of(destination));

        Destination found = destinationService.getById(1);

        assertEquals(destination.getName(), found.getName());
        verify(destinationRepository, times(1)).findById(1);
    }

    @Test
    public void testGetAllDestinations() {
        Destination destination1 = new Destination();
        destination1.setName("Test Destination 1");

        Destination destination2 = new Destination();
        destination2.setName("Test Destination 2");

        when(destinationRepository.findAll()).thenReturn(Arrays.asList(destination1, destination2));

        List<Destination> destinations = destinationService.getAll();

        assertEquals(2, destinations.size());
        verify(destinationRepository, times(1)).findAll();
    }

    @Test
    public void testUpdateDestination() {
        Destination destination = new Destination();
        destination.setId(1);
        destination.setName("Test Destination");

        when(destinationRepository.findById(1)).thenReturn(Optional.of(destination));
        when(destinationRepository.save(destination)).thenReturn(destination);

        Destination updated = destinationService.update(1, destination);

        assertEquals(destination.getName(), updated.getName());
        verify(destinationRepository, times(1)).findById(1);
        verify(destinationRepository, times(1)).save(destination);
    }

    @Test
    public void testDeleteDestination() {
        Destination destination = new Destination();
        destination.setId(1);

        destinationService.delete(1);

        verify(destinationRepository, times(1)).deleteById(1);
    }

    @Test
    public void testDeleteAllDestinations() {
        destinationService.deleteAll();

        verify(destinationRepository, times(1)).deleteAll();
    }
}