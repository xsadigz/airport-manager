package cz.fi.muni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication // same as @Configuration @EnableAutoConfiguration
public class FlightApplication {

    public static void main( String[] args )
    {
        SpringApplication.run(FlightApplication.class, args);
    }

}
