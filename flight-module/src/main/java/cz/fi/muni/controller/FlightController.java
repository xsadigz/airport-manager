package cz.fi.muni.controller;

import cz.fi.muni.dto.request.FlightCancelRequest;
import cz.fi.muni.dto.request.FlightChangeDestinationRequest;
import cz.fi.muni.dto.request.FlightCreateRequest;
import cz.fi.muni.dto.response.FlightCreateResponse;
import cz.fi.muni.dto.response.FlightDto;
import cz.fi.muni.facade.IFlightFacade;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RequestMapping("/api/flights/")
@RestController()
public class FlightController {

    private  final IFlightFacade flightFacade;
    public FlightController
            (IFlightFacade flightFacade) {
        this.flightFacade = flightFacade;
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public FlightDto getFlight(@PathVariable("id") Integer id) {
        return  flightFacade.getById(id);
    }
    @GetMapping("list")
    @ResponseStatus(HttpStatus.OK)
    public List<FlightDto> getAll() {
        return  flightFacade.getAll();
    }

    @PostMapping("create")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<FlightCreateResponse> createFlight(@RequestBody FlightCreateRequest request) {

        var result = flightFacade.create(request);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PatchMapping("/cancel")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelFlight(@RequestBody FlightCancelRequest request) {
          flightFacade.cancel(request);
    }

    @PatchMapping("/finish/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void finishFlight(@PathVariable("id") Integer id) {
          flightFacade.finish(id);
    }

    @PatchMapping("/start/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void startFlight(@PathVariable("id") Integer id) {
          flightFacade.start(id);
    }

    @PatchMapping("/delay/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delayFlight(@PathVariable("id") Integer id,
                            @RequestParam("minutes") Integer minutes) {
         flightFacade.delay(id, minutes);
    }

    @PatchMapping("/changeDestination")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changeDestination(@RequestBody FlightChangeDestinationRequest request) {
          flightFacade.changeDestination(request);
    }

    @GetMapping("availability")
    @ResponseStatus(HttpStatus.OK)
    public boolean isAirplaneAvailableForTimeRange(@RequestParam("airplaneId") Integer airplaneId,
                                                   @RequestParam("startTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date startTime,
                                                   @RequestParam("endTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endTime) {
        return flightFacade.isAirplaneAvailableForTimeRange(airplaneId, startTime, endTime);
    }


}
