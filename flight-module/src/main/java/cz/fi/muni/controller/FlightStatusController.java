package cz.fi.muni.controller;

import cz.fi.muni.dto.request.FlightStatusRequest;
import cz.fi.muni.dto.response.FlightStatusResponse;
import cz.fi.muni.facade.FlightStatusFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/flight-statuses/")
@RestController()
public class FlightStatusController {

    private final FlightStatusFacade flightFacade;

    private FlightStatusController(FlightStatusFacade flightFacade)
    {
        this.flightFacade = flightFacade;
    }

    @GetMapping("list")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<FlightStatusResponse>> getAll(){

        return ResponseEntity.ok(flightFacade.getAll());
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
   public ResponseEntity<FlightStatusResponse> getById(@PathVariable("id") Integer id) {

        return ResponseEntity.ok(flightFacade.getById(id));
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<FlightStatusResponse> getByName(@RequestParam("name") String name) {

        return ResponseEntity.ok(flightFacade.getByName(name));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    private void createNew(@RequestBody FlightStatusRequest request)
    {
        flightFacade.create(request);
    }


}
