package cz.fi.muni.data.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "Flight")
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ArrivalTime")
    private Date arrivalTime;


    @Column(name = "DepartureTime")
    private Date departureTime;

    @ManyToOne
    @JoinColumn(name = "StatusId")
    private FlightStatus status;

    @Column(name = "DestinationId")
    private Integer destinationId;

    @Column(name = "OriginId")
    private Integer originId;

    @ElementCollection
    @CollectionTable(
            name = "Flight_Steward",
            joinColumns = @JoinColumn(name = "FlightId")
    )
    @Column(name = "StewardId")
    private List<Integer> stewardIds;

    @Column(name = "AirplaneId")
    private Integer airplaneId;
}
