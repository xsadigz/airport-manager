package cz.fi.muni.data.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "FlightStatus")
@Data
public class FlightStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Status", length = 50, nullable = false, unique = true)
    private String status;

    public static FlightStatus valueOf(String status) {
        FlightStatus flightStatus = new FlightStatus();
        flightStatus.setStatus(status);
        return flightStatus;
    }

//    public static FlightStatus valueOf(String status) {
//        return status;
//    }
}
