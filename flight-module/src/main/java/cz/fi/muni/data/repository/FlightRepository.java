package cz.fi.muni.data.repository;


import cz.fi.muni.data.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Integer> {

    @Query("SELECT f FROM Flight f ORDER BY f.departureTime DESC")
    List<Flight> findAllOrderByDepartureTimeDesc();

    @Query("SELECT f FROM Flight f WHERE f.airplaneId = :airplaneId AND ((f.departureTime BETWEEN :startTime AND :endTime) OR (f.arrivalTime BETWEEN :startTime AND :endTime))")
    List<Flight> findFlightsByAirplaneIdAndTimeRange(int airplaneId, Date startTime, Date endTime);


}
