package cz.fi.muni.data.repository;

import cz.fi.muni.data.entity.FlightStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FlightStatusRepository extends JpaRepository<FlightStatus, Integer> {

    Optional<FlightStatus> findFlightStatusByStatus(String integer);
}
