package cz.fi.muni.dto.request;


import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class FlightCancelRequest {
    private  Integer flightId;
    private  Integer cancellationReasonId;
    private  String note;
}
