package cz.fi.muni.dto.request;

import lombok.Data;

@Data
public class FlightChangeDestinationRequest {

    private Integer flightId;
    private Integer newDestinationId;
}
