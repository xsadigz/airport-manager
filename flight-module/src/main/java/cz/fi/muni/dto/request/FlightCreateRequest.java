package cz.fi.muni.dto.request;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FlightCreateRequest {

    private Date arrivalTime;

//    private Integer statusId;
    private Date departureTime;
    private Integer destinationId;

    private Integer originId;

    private List<Integer> stewardIds;

    private Integer airplaneId;

}
