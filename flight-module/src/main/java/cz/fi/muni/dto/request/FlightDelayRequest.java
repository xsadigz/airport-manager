package cz.fi.muni.dto.request;

import lombok.Data;

@Data
public class FlightDelayRequest {

    private Integer flightId;
    private Integer delayInMinutes;
}
