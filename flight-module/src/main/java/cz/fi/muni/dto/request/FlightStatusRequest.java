package cz.fi.muni.dto.request;

import lombok.Data;

@Data
public class FlightStatusRequest {

    private String status;
}
