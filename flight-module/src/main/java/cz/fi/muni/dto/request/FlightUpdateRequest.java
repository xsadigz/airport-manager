package cz.fi.muni.dto.request;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FlightUpdateRequest {

    private Integer id;
    private Date arrivalTime;

    private String status;

    private Integer destinationId;

    private Integer originId;

    private List<Integer> stewardIds;

    private Integer airplaneId;

}
