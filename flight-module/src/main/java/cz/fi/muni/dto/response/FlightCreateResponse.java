package cz.fi.muni.dto.response;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FlightCreateResponse {
    private Integer id;
    private Date arrivalTime;
    private Date departureTime;
    private Long destinationId;
    private Long originId;
    private String status;
    private List<Integer> stewardIds;
    private Long airplaneId;
}
