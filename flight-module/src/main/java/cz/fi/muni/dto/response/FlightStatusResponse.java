package cz.fi.muni.dto.response;

import lombok.Data;

@Data
public class FlightStatusResponse {
    private Integer id;
    private String status;
}
