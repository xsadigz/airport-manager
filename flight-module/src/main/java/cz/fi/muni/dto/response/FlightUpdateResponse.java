package cz.fi.muni.dto.response;

import java.util.Date;
import java.util.List;

// TODO: Add missing annotations
public class FlightUpdateResponse {

    private Integer id;
    private Date arrivalTime;

    private String status;

    private Integer destinationId;

    private Integer originId;

    private List<Integer> stewardIds;

    private Integer airplaneId;

}
