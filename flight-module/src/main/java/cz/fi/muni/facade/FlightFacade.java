package cz.fi.muni.facade;

import cz.fi.muni.data.entity.Flight;
import cz.fi.muni.dto.request.FlightCancelRequest;
import cz.fi.muni.dto.request.FlightChangeDestinationRequest;
import cz.fi.muni.dto.request.FlightCreateRequest;
import cz.fi.muni.dto.response.FlightCreateResponse;
import cz.fi.muni.dto.response.FlightDto;
import cz.fi.muni.mapper.FlightMapper;
import cz.fi.muni.microservice_clients.AirplaneServiceClient;
import cz.fi.muni.microservice_clients.StewardServiceClient;
import cz.fi.muni.service.FlightService;
import cz.fi.muni.validator.AirplaneServiceValidator;
import cz.fi.muni.validator.DestinationServiceValidator;
import cz.fi.muni.validator.StewardServiceValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FlightFacade implements IFlightFacade {

    private final FlightMapper flightMapper;
    private final FlightService flightService;
    private final AirplaneServiceValidator airplaneServiceValidator;
    private final DestinationServiceValidator destinationServiceValidator;
    private final StewardServiceValidator stewardServiceValidator;
    private final StewardServiceClient stewardServiceClient;
    private final AirplaneServiceClient airplaneServiceClient;

//    public FlightFacade(FlightMapper flightMapper, FlightService flightService,
//                        AirplaneServiceValidator airplaneServiceValidator,
//                        DestinationServiceValidator destinationServiceValidator,
//                        StewardServiceValidator stewardServiceValidator) {
//        this.flightMapper = flightMapper;
//        this.flightService = flightService;
//        this.airplaneServiceValidator = airplaneServiceValidator;
//        this.destinationServiceValidator = destinationServiceValidator;
//        this.stewardServiceValidator = stewardServiceValidator;
//    }

    @Override
    public FlightCreateResponse create(FlightCreateRequest requestDto) {
        validateFlightCreateRequest(requestDto);

        // assign steward and airplane id to ms-es
        stewardServiceClient.changeStewardAvailability(requestDto.getStewardIds(), false);

        airplaneServiceClient.changeAirplaneAvailability(List.of(requestDto.getAirplaneId()), false);
        // assign airplane id to ms-es

        Flight flight = flightMapper.toEntity(requestDto);
        return flightMapper.toCreateResponseDto(flightService.create(flight));
    }

    @Override
    public FlightDto getById(Integer id) {
        return flightMapper.toResponseDto(flightService.getById(id));
    }

    @Override
    public List<FlightDto> getAll() {
        return flightMapper.toResponseDtoList(flightService.getAll());
    }

    @Override
    public void cancel(FlightCancelRequest request) {
        stewardServiceClient.changeStewardAvailability(getById(request.getFlightId()).getStewardIds(), true);

        airplaneServiceClient.changeAirplaneAvailability(List.of(getById(request.getFlightId()).getAirplaneId()), false);

        flightService.cancel(request.getFlightId());
    }

    @Override
    public void finish(Integer flightId) {
        flightService.finish(flightId);
    }

    @Override
    public void start(Integer flightId) {
        flightService.start(flightId);
    }

    @Override
    public void delay(Integer flightId, Integer minutes) {
        flightService.delay(flightId, minutes);
    }

    @Override
    public void changeDestination(FlightChangeDestinationRequest request) {
        flightService.changeDestination(request.getFlightId(), request.getNewDestinationId());
    }

    @Override
    public boolean isAirplaneAvailableForTimeRange(Integer airplaneId, Date startTime, Date endTime) {
        return flightService.isAirplaneAvailableForTimeRange(airplaneId, startTime, endTime);
    }

    private void validateFlightCreateRequest(FlightCreateRequest requestDto) {
        if (!airplaneServiceValidator.isValid(requestDto.getAirplaneId())) {
            throw new IllegalArgumentException("Airplane is not valid");
        }
        if (!destinationServiceValidator.isValid(requestDto.getDestinationId())) {
            throw new IllegalArgumentException("Destination is not valid");
        }
        for (Integer stewardId : requestDto.getStewardIds()) {
            if (!stewardServiceValidator.isValid(stewardId)) {
                throw new IllegalArgumentException("Steward id " + stewardId + " is not valid");
            }
        }
        // Add more validation as needed
    }
}
