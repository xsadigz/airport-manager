package cz.fi.muni.facade;

import cz.fi.muni.dto.request.FlightStatusRequest;
import cz.fi.muni.dto.response.FlightStatusResponse;

import java.util.List;

public interface FlightStatusFacade {

    List<FlightStatusResponse> getAll();

    FlightStatusResponse getById(Integer id);

    FlightStatusResponse getByName (String name);

    void create(FlightStatusRequest request);
}
