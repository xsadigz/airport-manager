package cz.fi.muni.facade;

import cz.fi.muni.dto.request.FlightStatusRequest;
import cz.fi.muni.dto.response.FlightStatusResponse;
import cz.fi.muni.mapper.FlightStatusMapper;
import cz.fi.muni.service.FlightStatusService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightStatusFacadeImpl implements   FlightStatusFacade {

    private final FlightStatusService service;
    private final FlightStatusMapper mapper;

    private FlightStatusFacadeImpl(FlightStatusService service, FlightStatusMapper mapper) {

        this.service = service;
        this.mapper = mapper;
    }
    @Override
    public List<FlightStatusResponse> getAll() {
        return service.getAll().stream().map(mapper::toResponse).toList();
    }

    @Override
    public FlightStatusResponse getById(Integer id) {
        return mapper.toResponse(service.getById(id));
    }

    @Override
    public FlightStatusResponse getByName(String name) {
        return mapper.toResponse(service.getByStatus(name));
    }

    @Override
    public void create(FlightStatusRequest request) {
        var entity = mapper.toEntity(request);
        service.create(entity);
    }
}
