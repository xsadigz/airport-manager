package cz.fi.muni.facade;

import cz.fi.muni.dto.request.FlightCancelRequest;
import cz.fi.muni.dto.request.FlightChangeDestinationRequest;
import cz.fi.muni.dto.request.FlightCreateRequest;
import cz.fi.muni.dto.response.FlightCreateResponse;
import cz.fi.muni.dto.response.FlightDto;

import java.util.Date;
import java.util.List;

public interface IFlightFacade {

    FlightCreateResponse create(FlightCreateRequest requestDto);

    FlightDto getById(Integer id);

    List<FlightDto> getAll();

    void cancel(FlightCancelRequest request);

    void finish(Integer flightId);


    void start(Integer flightId);

    void delay(Integer flightId, Integer minutes);

    void changeDestination(FlightChangeDestinationRequest request);

    boolean isAirplaneAvailableForTimeRange(Integer airplaneId, Date startTime, Date endTime);

}
