package cz.fi.muni.mapper;

import cz.fi.muni.data.entity.Flight;
import cz.fi.muni.data.entity.FlightStatus;
import cz.fi.muni.dto.request.FlightCreateRequest;
import cz.fi.muni.dto.request.FlightUpdateRequest;
import cz.fi.muni.dto.response.FlightCreateResponse;
import cz.fi.muni.dto.response.FlightDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface FlightMapper {

    @Mappings({
            @Mapping(target = "status", source = "status"),
            @Mapping(target = "stewardIds", source = "stewardIds"),
            @Mapping(target = "destinationId", source = "destinationId"),
            @Mapping(target = "originId", source = "originId"),
            @Mapping(target = "airplaneId", source = "airplaneId")
    })
    Flight toEntity(FlightDto requestDto);

    @Mappings({
//            @Mapping(target = "status.id", source = "statusId"),
            @Mapping(target = "stewardIds", source = "stewardIds"),
            @Mapping(target = "destinationId", source = "destinationId"),
            @Mapping(target = "originId", source = "originId"),
            @Mapping(target = "airplaneId", source = "airplaneId"),
            @Mapping(target = "arrivalTime", source = "arrivalTime"),
            @Mapping(target = "departureTime", source = "departureTime")
    })
    Flight toEntity(FlightCreateRequest requestDto);

    @Mappings({
            @Mapping(target = "status", source = "status"),
            @Mapping(target = "stewardIds", source = "stewardIds"),
            @Mapping(target = "destinationId", source = "destinationId"),
            @Mapping(target = "originId", source = "originId"),
            @Mapping(target = "airplaneId", source = "airplaneId")
    })
    Flight toEntity(FlightUpdateRequest requestDto);

    @Mappings({
            @Mapping(target = "status", source = "status.status"),
    })
    FlightDto toResponseDto(Flight destination);

    @Mappings({
            @Mapping(target = "status", source = "status.status"),
//            @Mapping(target = "stewardIds", source = "stewardIds"),
//            @Mapping(target = "destinationId", source = "destinationId"),
//            @Mapping(target = "originId", source = "originId"),
//            @Mapping(target = "airplaneId", source = "airplaneId")
    })
    FlightCreateResponse toCreateResponseDto(Flight destination);

    List<FlightDto> toResponseDtoList(List<Flight> destinations);

    default FlightStatus map(String status) {
        // Your logic to map String status to FlightStatus
        return FlightStatus.valueOf(status);
    }
}
