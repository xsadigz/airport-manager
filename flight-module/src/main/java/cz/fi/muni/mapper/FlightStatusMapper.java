package cz.fi.muni.mapper;


import cz.fi.muni.data.entity.FlightStatus;
import cz.fi.muni.dto.request.FlightStatusRequest;
import cz.fi.muni.dto.response.FlightStatusResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface FlightStatusMapper {

    FlightStatus toEntity(FlightStatusRequest requestDto);
    FlightStatusResponse toResponse(FlightStatus flightStatus);

}
