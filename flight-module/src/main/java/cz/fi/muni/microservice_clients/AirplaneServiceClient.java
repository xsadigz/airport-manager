package cz.fi.muni.microservice_clients;

import cz.fi.muni.microservice_dtos.AirplaneResponseDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class AirplaneServiceClient {
    private final RestTemplate restTemplate;

    @Value("${AIRPLANE_API_URL}")
    private  String AIRPLANE_API_URL; // Updated URL

   // private static final String AIRPLANE_API_URL = "http://localhost:8082/api/airplanes";
    public AirplaneServiceClient(RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
    }
    public List<AirplaneResponseDto> getAllDestinations() {
        ResponseEntity<AirplaneResponseDto[]> response = restTemplate.getForEntity(AIRPLANE_API_URL, AirplaneResponseDto[].class);
        AirplaneResponseDto[] stewards = response.getBody();
        return stewards != null ? Arrays.asList(stewards) : null;
    }
    public Optional<AirplaneResponseDto> getDestinationById(Integer id) {

        String url = AIRPLANE_API_URL + "/" + id;
        ResponseEntity<AirplaneResponseDto> response = restTemplate.getForEntity(url, AirplaneResponseDto.class);
        return Optional.ofNullable(response.getBody());
    }
    public void changeAirplaneAvailability(List<Integer> airplaneIds, boolean available) {
        airplaneIds.forEach(id -> {
            String url = AIRPLANE_API_URL + "/" + id + "/availability?available=" + available;
            restTemplate.put(url, null);
        });
    }

}
