package  cz.fi.muni.microservice_clients;

import cz.fi.muni.microservice_dtos.DestinationResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class DestinationServiceClient {

    private final  RestTemplate restTemplate;

    private static final String DESTINATION_API_URL = "http://destination-module:8083/api/destinations";
//    private static final String DESTINATION_API_URL = "http://localhost:8081/api/destinations";
    public DestinationServiceClient(RestTemplate restTemplate)
    {
      this.restTemplate = restTemplate;
    }

    public List<DestinationResponseDto> getAllDestinations() {
        ResponseEntity<DestinationResponseDto[]> response = restTemplate.getForEntity(DESTINATION_API_URL, DestinationResponseDto[].class);
        DestinationResponseDto[] destinations = response.getBody();
        return destinations != null ? Arrays.asList(destinations) : null;
    }

    public Optional<DestinationResponseDto> getDestinationById(Integer id) {
        String url = DESTINATION_API_URL + "/" + id;
        ResponseEntity<DestinationResponseDto> response = restTemplate.getForEntity(url, DestinationResponseDto.class);
        return Optional.ofNullable(response.getBody());
    }

}
