package cz.fi.muni.microservice_clients;

import cz.fi.muni.microservice_dtos.StewardResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class StewardServiceClient {

    private final RestTemplate restTemplate;

    private static final String STEWARD_API_URL = "http://steward-module:8088/api/stewards";
//    private static final String STEWARD_API_URL = "http://localhost:8083/api/stewards";
    public StewardServiceClient(RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
    }

    public List<StewardResponseDto> getAllStewards() {
        ResponseEntity<StewardResponseDto[]> response = restTemplate.getForEntity(STEWARD_API_URL, StewardResponseDto[].class);
        StewardResponseDto[] stewards = response.getBody();
        return stewards != null ? Arrays.asList(stewards) : null;
    }

    public Optional<StewardResponseDto> getStewardById(Integer id) {
        String url = STEWARD_API_URL + "/" + id;
        ResponseEntity<StewardResponseDto> response = restTemplate.getForEntity(url, StewardResponseDto.class);
        return Optional.ofNullable(response.getBody());
    }

    public void changeStewardAvailability(List<Integer> stewardIds, boolean available) {
        stewardIds.forEach(id -> {
            String url = STEWARD_API_URL + "/" + id + "/availability?available=" + available;
            restTemplate.put(url, null);
        });
    }

}
