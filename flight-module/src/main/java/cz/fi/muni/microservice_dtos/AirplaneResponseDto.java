package cz.fi.muni.microservice_dtos;

import lombok.Data;

import java.time.LocalDate;

@Data
public class AirplaneResponseDto
{
    Integer id;
    String name;
    String type;
    Integer capacity;
    Double range;
    LocalDate maintenanceSchedule;
    Integer yearOfManufacture;
}
