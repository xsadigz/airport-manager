package cz.fi.muni.microservice_dtos;

import lombok.Data;

@Data
public class DestinationResponseDto {
    Integer id;
    String name;
    String country;
    String city;
    String address;
    String state;
    String country_code;
    String airport_size;
    Integer traffic_volume;
    Integer runways;

}
