package cz.fi.muni.microservice_dtos;

import lombok.Data;

@Data
public class StewardResponseDto {

    Integer id;
    String firstname;
    String lastname;
    String contactNumber;
    String email;
    String workSchedule;
}
