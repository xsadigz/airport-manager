package cz.fi.muni.service;

import cz.fi.muni.data.entity.Flight;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface FlightService {

    Flight create(Flight flight);

    Flight getById(Integer id);

    List<Flight> getAll();

    Flight update(Integer id, Flight destination);

    void cancel(Integer id);

    void changeDestination( Integer flightId,
     Integer newDestinationId);

    void finish(Integer flightId);

    void start(Integer flightId);

    void delay(Integer flightId, Integer minutes);

    boolean isAirplaneAvailableForTimeRange(Integer airplaneId, Date startTime, Date endTime);

}
