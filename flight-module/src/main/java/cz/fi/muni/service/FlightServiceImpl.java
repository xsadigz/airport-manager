package cz.fi.muni.service;

import cz.fi.muni.data.entity.Flight;
import cz.fi.muni.data.entity.FlightStatus;
import cz.fi.muni.data.repository.FlightRepository;
import cz.fi.muni.data.repository.FlightStatusRepository;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;
    private FlightStatusRepository flightStatusRepository;


    public FlightServiceImpl(FlightRepository flightRepository, FlightStatusRepository flightStatusRepository) {
        this.flightRepository = flightRepository;
        this.flightStatusRepository =  flightStatusRepository;
    }

    @Override
    public Flight create(Flight flight) {
        return flightRepository.save(flight);
    }

    @Override
    public Flight getById(Integer id) {
        return flightRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Flight with id " + id + " not found"));
    }

    @Override
    public List<Flight> getAll() {
        return flightRepository.findAllOrderByDepartureTimeDesc();
    }

    @Override
    public Flight update(Integer id, Flight flight) {
        flight.setId(id); // Ensure the provided ID matches the entity
        return flightRepository.save(flight);
    }

    @Override
    public void cancel(Integer id) {

        Flight flight = flightRepository.getReferenceById(id);

        // Fetch the FlightStatus entity corresponding to "Canceled"
        FlightStatus canceledStatus = flightStatusRepository.findFlightStatusByStatus("Canceled")
                .orElseThrow(() -> new IllegalStateException("Canceled status not found"));

        // Set the flight's status to the canceled status entity
        flight.setStatus(canceledStatus);

        flightRepository.save(flight);
    }

    @Override
    public void changeDestination(Integer flightId, Integer newDestinationId) {
        Flight flight = getById(flightId);
        // Assuming newDestinationId is validated and exists in the system
        flight.setDestinationId(newDestinationId);
        flightRepository.save(flight);
    }

    @Override
    public void finish(Integer flightId) {
        Flight flight = getById(flightId);
        FlightStatus finishedStatus = flightStatusRepository.findFlightStatusByStatus("Finished")
                .orElseThrow(() -> new IllegalStateException("Finished status not found"));
        flight.setStatus(finishedStatus);
        flightRepository.save(flight);
    }

    @Override
    public void start(Integer flightId) {
        Flight flight = getById(flightId);
        FlightStatus startedStatus = flightStatusRepository.findFlightStatusByStatus("Started")
                .orElseThrow(() -> new IllegalStateException("Started status not found"));
        flight.setStatus(startedStatus);
        flightRepository.save(flight);
    }

    @Override
    public void delay(Integer flightId, Integer minutes) {
        Flight flight = getById(flightId);
        FlightStatus delayedStatus = flightStatusRepository.findFlightStatusByStatus("Delayed")
                .orElseThrow(() -> new IllegalStateException("Delayed status not found"));
        flight.setStatus(delayedStatus);
        // Update departure time by adding minutes
        flight.setDepartureTime(addMinutesToDate(flight.getDepartureTime(), minutes));
        flightRepository.save(flight);
    }

    @Override
    public boolean isAirplaneAvailableForTimeRange(Integer airplaneId, Date startTime, Date endTime) {
        List<Flight> flights = flightRepository.findFlightsByAirplaneIdAndTimeRange(airplaneId, startTime, endTime);
        return flights.isEmpty();
    }

    // Utility method to add minutes to a date
    private Date addMinutesToDate(Date date, int minutes) {
        long milliseconds = date.getTime() + ((long) minutes * 60 * 1000);
        return new Date(milliseconds);
    }

}
