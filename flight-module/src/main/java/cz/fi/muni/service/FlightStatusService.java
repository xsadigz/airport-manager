package cz.fi.muni.service;

import cz.fi.muni.data.entity.FlightStatus;

import java.util.List;

public interface FlightStatusService {

    FlightStatus getById(Integer id);

    FlightStatus getByStatus(String status);

    void create(FlightStatus flightStatus);

    void update(FlightStatus flightStatus);

    List<FlightStatus> getAll();


}
