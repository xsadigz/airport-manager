package cz.fi.muni.service;

import cz.fi.muni.data.entity.FlightStatus;
import cz.fi.muni.data.repository.FlightStatusRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightStatusServiceImpl implements FlightStatusService {

    private final  FlightStatusRepository flightStatusRepository;
    private FlightStatusServiceImpl(FlightStatusRepository flightStatusRepository) {
        this.flightStatusRepository = flightStatusRepository;
    }
    @Override
    public FlightStatus getById(Integer id) {

        return  flightStatusRepository.findById(id).orElseThrow();
    }

    @Override
    public FlightStatus getByStatus(String status) {

        return flightStatusRepository.findFlightStatusByStatus(status).orElseThrow();
    }

    @Override
    public void create(FlightStatus flightStatus) {

        flightStatusRepository.saveAndFlush(flightStatus);
    }

    @Override
    public void update(FlightStatus flightStatus) {
            flightStatusRepository.save(flightStatus);
    }

    @Override
    public List<FlightStatus> getAll() {

        return flightStatusRepository.findAll();
    }
}
