package cz.fi.muni.validator;

import cz.fi.muni.microservice_clients.AirplaneServiceClient;
import org.springframework.stereotype.Service;

@Service
public class AirplaneServiceValidator  implements ServiceValidator{

    private final AirplaneServiceClient airplaneServiceClient;

    public AirplaneServiceValidator(AirplaneServiceClient airplaneServiceClient)
    {
        this.airplaneServiceClient = airplaneServiceClient;
    }

    public boolean isValid(Integer id)
    {
        return airplaneServiceClient.getDestinationById(id).isPresent();
    }
}
