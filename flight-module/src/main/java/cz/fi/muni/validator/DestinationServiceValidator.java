package cz.fi.muni.validator;

import cz.fi.muni.microservice_clients.DestinationServiceClient;
import org.springframework.stereotype.Service;

@Service
public class DestinationServiceValidator implements ServiceValidator {

    private final DestinationServiceClient destinationServiceClient;

    public DestinationServiceValidator(DestinationServiceClient destinationServiceClient)
    {
        this.destinationServiceClient = destinationServiceClient;
    }
    public boolean isValid(Integer id)
    {
        return destinationServiceClient.getDestinationById(id).isPresent();
    }
}
