package cz.fi.muni.validator;

public interface ServiceValidator {
    boolean isValid(Integer id);
}
