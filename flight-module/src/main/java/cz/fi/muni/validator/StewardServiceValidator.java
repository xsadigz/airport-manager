package cz.fi.muni.validator;

import cz.fi.muni.microservice_clients.StewardServiceClient;
import org.springframework.stereotype.Service;

@Service
public class StewardServiceValidator implements ServiceValidator{

    private final StewardServiceClient stewardServiceClient;

    public StewardServiceValidator(StewardServiceClient stewardServiceClient)
    {
        this.stewardServiceClient = stewardServiceClient;
    }
    @Override
    public boolean isValid(Integer id) {
        return stewardServiceClient.getStewardById(id).isPresent();
    }
}
