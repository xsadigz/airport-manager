package cz.fi.muni.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.fi.muni.dto.request.FlightCancelRequest;
import cz.fi.muni.dto.request.FlightChangeDestinationRequest;
import cz.fi.muni.dto.request.FlightCreateRequest;
import cz.fi.muni.dto.response.FlightCreateResponse;
import cz.fi.muni.dto.response.FlightDto;
import cz.fi.muni.facade.IFlightFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
@ExtendWith({SpringExtension.class, MockitoExtension.class})
@WebMvcTest(FlightController.class)
@AutoConfigureMockMvc
class FlightControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private IFlightFacade flightFacade;

    @Test
    void getFlight() throws Exception {
        int flightId = 1;
        FlightDto flightDto = new FlightDto();
        flightDto.setId(flightId);

        when(flightFacade.getById(flightId)).thenReturn(flightDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/flights/{id}", flightId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(flightId));
    }

    @Test
    void getAll() throws Exception {
        FlightDto flightDto = new FlightDto();
        flightDto.setId(1);

        when(flightFacade.getAll()).thenReturn(Collections.singletonList(flightDto));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/flights/list"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1));
    }

    @Test
    void createFlight() throws Exception {
        FlightCreateRequest request = new FlightCreateRequest();
        FlightCreateResponse response = new FlightCreateResponse();
        response.setId(1);

        when(flightFacade.create(any(FlightCreateRequest.class))).thenReturn(response);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/flights/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    void cancelFlight() throws Exception {
        FlightCancelRequest request = new FlightCancelRequest();

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/flights/cancel")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void finishFlight() throws Exception {
        int flightId = 1;

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/flights/finish/{id}", flightId))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void startFlight() throws Exception {
        int flightId = 1;

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/flights/start/{id}", flightId))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void delayFlight() throws Exception {
        int flightId = 1;
        int minutes = 30;

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/flights/delay/{id}", flightId)
                        .param("minutes", String.valueOf(minutes)))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void changeDestination() throws Exception {
        FlightChangeDestinationRequest request = new FlightChangeDestinationRequest();

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/flights/changeDestination")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}