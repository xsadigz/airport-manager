// FlightFacadeTest.java
package cz.fi.muni.facade;

import cz.fi.muni.data.entity.Flight;
import cz.fi.muni.dto.request.FlightCancelRequest;
import cz.fi.muni.dto.request.FlightCreateRequest;
import cz.fi.muni.mapper.FlightMapper;
import cz.fi.muni.service.FlightServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.mockito.Mockito.*;

public class FlightFacadeTest {

    @Mock
    private FlightMapper flightMapper;

    @Mock
    private FlightServiceImpl flightServiceImpl;

    @InjectMocks
    private FlightFacade flightFacade;

    @Before
    public void setup()
    {
        MockitoAnnotations.openMocks(this); //without this you will get NPE
    }


    @Test
    public void createFlightTest() {
        FlightCreateRequest flightCreateRequest = new FlightCreateRequest();
        flightCreateRequest.setArrivalTime(new Date());
        flightCreateRequest.setDestinationId(1);
        flightCreateRequest.setOriginId(1);
        flightCreateRequest.setStewardIds(Arrays.asList(1, 2));
        flightCreateRequest.setAirplaneId(1);

        Flight flight = new Flight();
        flight.setArrivalTime(flightCreateRequest.getArrivalTime());
        flight.setDepartureTime(flightCreateRequest.getDepartureTime());

        when(flightMapper.toEntity(any(FlightCreateRequest.class))).thenReturn(flight);
        flightFacade.create(flightCreateRequest);
        verify(flightServiceImpl, times(1)).create(any(Flight.class));
    }

    @Test
    public void getByIdTest() {
        when(flightServiceImpl.getById(any(Integer.class))).thenReturn(new Flight());

        flightFacade.getById(1);

        verify(flightServiceImpl, times(1)).getById(any(Integer.class));
    }

    @Test
    public void getAllTest() {
        when(flightServiceImpl.getAll()).thenReturn(Collections.emptyList());

        flightFacade.getAll();

        verify(flightServiceImpl, times(1)).getAll();
    }

    @Test
    public void cancelTest() {
        FlightCancelRequest flightCancelRequest = new FlightCancelRequest();
        flightCancelRequest.setFlightId(1);
        flightCancelRequest.setCancellationReasonId(1);
        flightCancelRequest.setNote("Note");

        flightFacade.cancel(flightCancelRequest);

        verify(flightServiceImpl, times(1)).cancel(any(Integer.class));
        assert(true);
    }


}