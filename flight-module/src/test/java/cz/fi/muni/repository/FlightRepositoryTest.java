package cz.fi.muni.repository;

import cz.fi.muni.data.entity.Flight;
import cz.fi.muni.data.entity.FlightStatus;
import cz.fi.muni.data.repository.FlightRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;



    @DataJpaTest
    public class FlightRepositoryTest {

        @Autowired
        private FlightRepository flightRepository;

        @Autowired
        private TestEntityManager entityManager;

        @Test
        public void testSaveFlight() {
            // Given
            FlightStatus flightStatus = new FlightStatus();
            flightStatus.setStatus("Scheduled");
            entityManager.persist(flightStatus);
            entityManager.flush();

            Flight flight = new Flight();
            flight.setArrivalTime(new Date());
            flight.setDepartureTime(new Date());
            flight.setStatus(flightStatus);
            flight.setDestinationId(1);
            flight.setOriginId(2);
            flight.setStewardIds(List.of(1, 2, 3));
            flight.setAirplaneId(1);

            // When
            Flight savedFlight = flightRepository.save(flight);

            // Then
            assertThat(savedFlight.getId()).isNotNull();
            assertThat(savedFlight.getArrivalTime()).isEqualTo(flight.getArrivalTime());
            assertThat(savedFlight.getDepartureTime()).isEqualTo(flight.getDepartureTime());
            assertThat(savedFlight.getStatus()).isEqualTo(flight.getStatus());
            assertThat(savedFlight.getDestinationId()).isEqualTo(flight.getDestinationId());
            assertThat(savedFlight.getOriginId()).isEqualTo(flight.getOriginId());
            assertThat(savedFlight.getStewardIds()).isEqualTo(flight.getStewardIds());
            assertThat(savedFlight.getAirplaneId()).isEqualTo(flight.getAirplaneId());
        }

        @Test
        public void testUpdateFlight() {
            // Given
            FlightStatus flightStatus = new FlightStatus();
            flightStatus.setStatus("Scheduled");
            entityManager.persist(flightStatus);
            entityManager.flush();

            Flight flight = new Flight();
            flight.setArrivalTime(new Date());
            flight.setDepartureTime(new Date());
            flight.setStatus(flightStatus);
            flight.setDestinationId(1);
            flight.setOriginId(2);
            flight.setStewardIds(List.of(1, 2, 3));
            flight.setAirplaneId(1);

            Flight savedFlight = entityManager.persistAndFlush(flight);

            // When

            Flight toBeupdatedFlight = flightRepository.getReferenceById(savedFlight.getId());
            toBeupdatedFlight.setStewardIds(new ArrayList<>(List.of(1, 2, 4))); // Add a new steward
            toBeupdatedFlight.setAirplaneId(2);

            // Save the updated flight
            Flight updatedSavedFlight = flightRepository.save(toBeupdatedFlight); // Use merge instead of save

            // Then
            assertThat(updatedSavedFlight.getAirplaneId()).isEqualTo(2);
            assertThat(updatedSavedFlight.getStewardIds()).contains(4); // Ensure the new steward is added
        }

        @Test
        public void testGetFlightById() {
            // Given
            FlightStatus flightStatus = new FlightStatus();
            flightStatus.setStatus("Scheduled");
            entityManager.persist(flightStatus);
            entityManager.flush();

            Flight flight = new Flight();
            flight.setArrivalTime(new Date());
            flight.setDepartureTime(new Date());
            flight.setStatus(flightStatus);
            flight.setDestinationId(1);
            flight.setOriginId(2);
            flight.setStewardIds(List.of(1, 2, 3));
            flight.setAirplaneId(1);

            Flight savedFlight = entityManager.persistAndFlush(flight);

            // When
            Flight foundFlight = flightRepository.findById(savedFlight.getId()).orElse(null);

            // Then
            assertThat(foundFlight).isNotNull();
            assertThat(foundFlight.getId()).isEqualTo(savedFlight.getId());
        }

        @Test
        public void testGetAllFlights() {
            // Given
            FlightStatus flightStatus = new FlightStatus();
            flightStatus.setStatus("Scheduled");
            entityManager.persist(flightStatus);
            entityManager.flush();

            Flight flight1 = new Flight();
            flight1.setArrivalTime(new Date());
            flight1.setDepartureTime(new Date());
            flight1.setStatus(flightStatus);
            flight1.setDestinationId(1);
            flight1.setOriginId(2);
            flight1.setStewardIds(List.of(1, 2, 3));
            flight1.setAirplaneId(1);

            Flight flight2 = new Flight();
            flight2.setArrivalTime(new Date());
            flight2.setDepartureTime(new Date());
            flight2.setStatus(flightStatus);
            flight2.setDestinationId(3);
            flight2.setOriginId(4);
            flight2.setStewardIds(List.of(4, 5, 6));
            flight2.setAirplaneId(2);

            entityManager.persistAndFlush(flight1);
            entityManager.persistAndFlush(flight2);

            // When
            List<Flight> flights = flightRepository.findAll();

            // Then
            assertThat(flights).isNotNull();
            assertThat(flights.size()).isEqualTo(2);
        }

        // Add more test methods as needed
}
