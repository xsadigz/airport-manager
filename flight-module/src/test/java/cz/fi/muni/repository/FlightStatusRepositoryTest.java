package cz.fi.muni.repository;

import cz.fi.muni.data.entity.FlightStatus;
import cz.fi.muni.data.repository.FlightStatusRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class FlightStatusRepositoryTest {

    @Autowired
    private FlightStatusRepository flightStatusRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void testSaveFlightStatus() {
        // Given
        FlightStatus flightStatus = FlightStatus.valueOf("Scheduled");

        // When
        FlightStatus savedFlightStatus = flightStatusRepository.save(flightStatus);

        // Then
        assertThat(savedFlightStatus.getId()).isNotNull();
        assertThat(savedFlightStatus.getStatus()).isEqualTo("Scheduled");
    }

    @Test
    public void testFindFlightStatusByStatus() {
        // Given
        FlightStatus flightStatus = FlightStatus.valueOf("Scheduled");
        entityManager.persistAndFlush(flightStatus);

        // When
        Optional<FlightStatus> foundFlightStatusOptional = flightStatusRepository.findFlightStatusByStatus("Scheduled");

        // Then
        assertThat(foundFlightStatusOptional).isPresent();
        FlightStatus foundFlightStatus = foundFlightStatusOptional.get();
        assertThat(foundFlightStatus.getId()).isEqualTo(flightStatus.getId());
        assertThat(foundFlightStatus.getStatus()).isEqualTo("Scheduled");
    }

    @Test
    public void testUpdateFlightStatus() {
        // Given
        FlightStatus flightStatus = FlightStatus.valueOf("Scheduled");
        entityManager.persistAndFlush(flightStatus);

        // When
        flightStatus.setStatus("Delayed");
        FlightStatus updatedFlightStatus = flightStatusRepository.save(flightStatus);

        // Then
        assertThat(updatedFlightStatus.getStatus()).isEqualTo("Delayed");
    }

    @Test
    public void testDeleteFlightStatus() {
        // Given
        FlightStatus flightStatus = FlightStatus.valueOf("Scheduled");
        entityManager.persistAndFlush(flightStatus);

        // When
        flightStatusRepository.delete(flightStatus);

        // Then
        assertThat(flightStatusRepository.findById(flightStatus.getId())).isEmpty();
    }
}