package cz.fi.muni.service;

import cz.fi.muni.data.entity.Flight;
import cz.fi.muni.data.entity.FlightStatus;
import cz.fi.muni.data.repository.FlightRepository;
import cz.fi.muni.data.repository.FlightStatusRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FlightServiceImplTest {

    @Mock
    private FlightRepository flightRepository;

    @Mock
    private FlightStatusRepository flightStatusRepository;

    @InjectMocks
    private FlightServiceImpl flightService;

    @Test
    void create() {
        // Given
        Flight flight = new Flight();
        when(flightRepository.save(flight)).thenReturn(flight);

        // When
        Flight createdFlight = flightService.create(flight);

        // Then
        assertEquals(flight, createdFlight);
        verify(flightRepository, times(1)).save(flight);
    }

    @Test
    void getById_existingFlight() {
        // Given
        Integer flightId = 1;
        Flight flight = new Flight();
        when(flightRepository.findById(flightId)).thenReturn(Optional.of(flight));

        // When
        Flight foundFlight = flightService.getById(flightId);

        // Then
        assertNotNull(foundFlight);
        assertEquals(flight, foundFlight);
    }

    @Test
    void getById_nonExistingFlight() {
        // Given
        Integer flightId = 1;
        when(flightRepository.findById(flightId)).thenReturn(Optional.empty());

        // When/Then
        assertThrows(IllegalArgumentException.class, () -> flightService.getById(flightId));
    }

    @Test
    void getAll() {
        // Given
        List<Flight> flights = Arrays.asList(new Flight(), new Flight());
        when(flightService.getAll()).thenReturn(flights);

        // When
        List<Flight> foundFlights = flightService.getAll();

        // Then
        Assertions.assertEquals(flights.size(), foundFlights.size());
        assertTrue(foundFlights.containsAll(flights));
    }

    @Test
    void update() {
        // Given
        Integer flightId = 1;
        Flight flight = new Flight();
        flight.setId(flightId);
        when(flightRepository.save(flight)).thenReturn(flight);

        // When
        Flight updatedFlight = flightService.update(flightId, flight);

        // Then
        assertEquals(flight, updatedFlight);
        verify(flightRepository, times(1)).save(flight);
    }

    @Test
    void cancel() {
        // Given
        Integer flightId = 1;
        Flight flight = new Flight();
        when(flightRepository.getReferenceById(flightId)).thenReturn(flight);

        FlightStatus canceledStatus = new FlightStatus();
        when(flightStatusRepository.findFlightStatusByStatus("Canceled")).thenReturn(Optional.of(canceledStatus));

        // When
        flightService.cancel(flightId);

        // Then
        assertEquals(canceledStatus, flight.getStatus());
        verify(flightRepository, times(1)).save(flight);
    }

    @Test
    void changeDestination() {
        // Given
        Integer flightId = 1;
        Integer newDestinationId = 2;
        Flight flight = new Flight();
        when(flightRepository.findById(flightId)).thenReturn(Optional.of(flight));

        // When
        flightService.changeDestination(flightId, newDestinationId);

        // Then
        assertEquals(newDestinationId, flight.getDestinationId());
        verify(flightRepository, times(1)).save(flight);
    }

    @Test
    void finish() {
        // Given
        Integer flightId = 1;
        Flight flight = new Flight();
        when(flightRepository.findById(flightId)).thenReturn(Optional.of(flight));

        FlightStatus finishedStatus = new FlightStatus();
        when(flightStatusRepository.findFlightStatusByStatus("Finished")).thenReturn(Optional.of(finishedStatus));

        // When
        flightService.finish(flightId);

        // Then
        assertEquals(finishedStatus, flight.getStatus());
        verify(flightRepository, times(1)).save(flight);
    }

    @Test
    void start() {
        // Given
        Integer flightId = 1;
        Flight flight = new Flight();
        when(flightRepository.findById(flightId)).thenReturn(Optional.of(flight));

        FlightStatus startedStatus = new FlightStatus();
        when(flightStatusRepository.findFlightStatusByStatus("Started")).thenReturn(Optional.of(startedStatus));

        // When
        flightService.start(flightId);

        // Then
        assertEquals(startedStatus, flight.getStatus());
        verify(flightRepository, times(1)).save(flight);
    }

    @Test
    void delay() {
        // Given
        Integer flightId = 1;
        Integer minutes = 30;
        Date initialDepartureTime = new Date();
        Flight flight = new Flight();
        flight.setId(flightId);
        flight.setDepartureTime(initialDepartureTime);

        when(flightRepository.findById(flightId)).thenReturn(Optional.of(flight));

        FlightStatus delayedStatus = new FlightStatus();
        when(flightStatusRepository.findFlightStatusByStatus("Delayed")).thenReturn(Optional.of(delayedStatus));

        // When
        flightService.delay(flightId, minutes);

        // Then
        verify(flightRepository, times(1)).save(flight);

        // Check if the departure time is updated correctly
        long expectedMilliseconds = initialDepartureTime.getTime() + (minutes * 60 * 1000);
        assertEquals(expectedMilliseconds, flight.getDepartureTime().getTime());
    }
}
