package cz.fi.muni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class StewardServiceApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(StewardServiceApp.class, args);
    }
}
