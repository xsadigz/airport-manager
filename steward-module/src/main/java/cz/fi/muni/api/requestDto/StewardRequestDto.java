package cz.fi.muni.api.requestDto;

import cz.fi.muni.data.model.WorkSchedule;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StewardRequestDto {

    String firstname;
    String lastname;
    String contactNumber;
    String email;
    boolean available;

}
