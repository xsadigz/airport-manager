package cz.fi.muni.api.requestDto;

import com.fasterxml.jackson.annotation.JsonFormat;
import cz.fi.muni.data.model.DayOfWeek;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkDayRequestDto {

    DayOfWeek day;
    LocalTime shiftStart;
    LocalTime shiftEnd;
    Integer breakHour;

}
