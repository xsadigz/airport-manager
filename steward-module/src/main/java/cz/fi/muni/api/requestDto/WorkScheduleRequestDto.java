package cz.fi.muni.api.requestDto;

import cz.fi.muni.data.model.Steward;
import cz.fi.muni.data.model.WorkDay;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkScheduleRequestDto {

    List<WorkDayRequestDto> workDays;
    boolean temporary;
    List<Integer> stewardIds;

}
