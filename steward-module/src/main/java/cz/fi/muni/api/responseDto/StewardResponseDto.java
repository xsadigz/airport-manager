package cz.fi.muni.api.responseDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StewardResponseDto {

    Integer id;
    String firstname;
    String lastname;
    String contactNumber;
    String email;
    WorkScheduleResponseDto workSchedule;
    boolean available;

}
