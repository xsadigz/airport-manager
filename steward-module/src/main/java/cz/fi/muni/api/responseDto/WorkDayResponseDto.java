package cz.fi.muni.api.responseDto;

import cz.fi.muni.data.model.DayOfWeek;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkDayResponseDto {

    DayOfWeek day;
    LocalTime shiftStart;
    LocalTime shiftEnd;
    Integer breakHour;

}
