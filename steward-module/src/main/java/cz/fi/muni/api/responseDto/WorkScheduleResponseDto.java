package cz.fi.muni.api.responseDto;

import cz.fi.muni.data.model.Steward;
import cz.fi.muni.data.model.WorkDay;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkScheduleResponseDto {

    List<WorkDayResponseDto> workDays;
    boolean temporary;

}
