package cz.fi.muni.controller;


import cz.fi.muni.api.requestDto.StewardRequestDto;
import cz.fi.muni.api.responseDto.StewardResponseDto;
import cz.fi.muni.facade.IStewardFacade;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalTime;
import java.util.List;

@RestController
@RequestMapping("/api/stewards")
public class StewardController {

    private static final Logger log = LoggerFactory.getLogger(StewardController.class);

    private final IStewardFacade stewardFacade;

    @Autowired
    public StewardController(IStewardFacade stewardFacade) {
        this.stewardFacade = stewardFacade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addSteward(@RequestBody StewardRequestDto requestDto) {
        stewardFacade.create(requestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSteward(@PathVariable Integer id) {
        if (id == null) {
            log.info("Provided steward identifier is null.");
        }
        stewardFacade.delete(id);
    }


    @GetMapping
    public ResponseEntity<List<StewardResponseDto>> getAllStewards() {
        log.info("getAllStewards");
        return new ResponseEntity<>(stewardFacade.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StewardResponseDto> getSteward(@PathVariable Integer id) {
        if (id == null) {
            log.info("Provided steward identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(stewardFacade.getById(id), HttpStatus.OK);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Void> editSteward(@PathVariable Integer id, @RequestBody StewardRequestDto requestDto) {
        stewardFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{id}/availability")
    public ResponseEntity<StewardResponseDto> changeAvailability(@PathVariable Integer id, @RequestParam boolean available) {
        return new ResponseEntity<>(stewardFacade.changeAvailability(id, available), HttpStatus.OK);
    }

    @GetMapping("/{id}/availability")
    public ResponseEntity<Boolean> isStewardAvailableForTimeRange(@PathVariable Integer id, @RequestParam LocalTime startTime, @RequestParam LocalTime endTime) {
        return new ResponseEntity<>(stewardFacade.isStewardAvailableForTimeRange(id, startTime, endTime), HttpStatus.OK);
    }

}
