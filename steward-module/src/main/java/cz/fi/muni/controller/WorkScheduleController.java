package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.WorkScheduleRequestDto;
import cz.fi.muni.api.responseDto.WorkScheduleResponseDto;
import cz.fi.muni.facade.IWorkScheduleFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/workSchedules")
public class WorkScheduleController {

    private final IWorkScheduleFacade workScheduleFacade;

    @Autowired
    public WorkScheduleController(IWorkScheduleFacade workScheduleFacade) {
        this.workScheduleFacade = workScheduleFacade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addWorkSchedule(@RequestBody WorkScheduleRequestDto requestDto) {
        workScheduleFacade.create(requestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteWorkSchedule(@PathVariable Integer id) {
        workScheduleFacade.delete(id);
    }


    @GetMapping
    public ResponseEntity<List<WorkScheduleResponseDto>> getAllWorkSchedules() {
        return new ResponseEntity<>(workScheduleFacade.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<WorkScheduleResponseDto> getWorkSchedule(@PathVariable Integer id) {
        return new ResponseEntity<>(workScheduleFacade.getById(id), HttpStatus.OK);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Void> editWorkSchedule(@PathVariable Integer id, @RequestBody WorkScheduleRequestDto requestDto) {
        workScheduleFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
