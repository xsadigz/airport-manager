package cz.fi.muni.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Steward implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @NotBlank(message = "First name cannot be blank")
    String firstname;
    @NotBlank(message = "Last name cannot be blank")
    String lastname;
    @NotBlank(message = "Contact number cannot be blank")
    String contactNumber;
    @Email(message = "Email should be valid")
    String email;
    @ManyToOne
    WorkSchedule workSchedule;
    boolean available;

}
