package cz.fi.muni.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Embeddable
public class WorkDay implements Serializable {

    @NotNull(message = "Day cannot be null")
    @Enumerated(EnumType.STRING)
    DayOfWeek day;

    @FutureOrPresent(message = "Shift start cannot be in the past")
    LocalTime shiftStart;

    @Future(message = "Shift end must be in the future")
    LocalTime shiftEnd;

    @Min(value = 1, message = "Break hour must be at least 1")
    Integer breakHour;

}
