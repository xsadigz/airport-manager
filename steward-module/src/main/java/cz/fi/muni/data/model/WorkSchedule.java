package cz.fi.muni.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkSchedule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @NotEmpty(message = "Steward list cannot be empty")
    @OneToMany
    List<Steward> stewards;

    @NotEmpty(message = "Work days list cannot be empty")
    @ElementCollection
    @CollectionTable(name = "work_days", joinColumns = @JoinColumn(name = "schedule_id"))
    List<WorkDay> workDays;

    boolean temporary;

}
