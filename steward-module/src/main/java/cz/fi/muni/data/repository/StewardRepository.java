package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.Steward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface StewardRepository extends JpaRepository<Steward, Integer> {

    @Query("SELECT s FROM Steward s JOIN s.workSchedule ws JOIN ws.workDays wd WHERE wd.day = :day AND wd.shiftStart <= :startTime AND wd.shiftEnd >= :endTime")
    List<Steward> findStewardsByIdAndTimeRange(int planeId, LocalDateTime startTime, LocalDateTime endTime);

}
