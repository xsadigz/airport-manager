package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.WorkSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkScheduleRepository extends JpaRepository<WorkSchedule, Integer> {

}
