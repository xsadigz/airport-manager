package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.StewardRequestDto;
import cz.fi.muni.api.responseDto.StewardResponseDto;
import cz.fi.muni.data.model.Steward;

import java.time.LocalTime;
import java.util.List;

public interface IStewardFacade {

    void create(StewardRequestDto requestDto);

    StewardResponseDto getById(Integer id);

    List<StewardResponseDto> getAll();

    void update(Integer id, StewardRequestDto requestDto);

    void delete(Integer id);

    boolean isStewardAvailableForTimeRange(Integer stewardId, LocalTime startTime, LocalTime endTime);

    StewardResponseDto changeAvailability(Integer id, boolean available);

}
