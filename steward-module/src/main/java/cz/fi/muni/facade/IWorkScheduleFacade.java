package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.WorkScheduleRequestDto;
import cz.fi.muni.api.responseDto.WorkScheduleResponseDto;

import java.util.List;

public interface IWorkScheduleFacade {

    void create(WorkScheduleRequestDto requestDto);

    WorkScheduleResponseDto getById(Integer id);

    List<WorkScheduleResponseDto> getAll();

    void update(Integer id, WorkScheduleRequestDto requestDto);

    void delete(Integer id);

}
