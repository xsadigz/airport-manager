package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.StewardRequestDto;
import cz.fi.muni.api.responseDto.StewardResponseDto;
import cz.fi.muni.data.model.Steward;
import cz.fi.muni.mapper.StewardMapper;
import cz.fi.muni.service.StewardService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StewardFacade implements IStewardFacade {

    private final StewardMapper stewardMapper;
    private final StewardService stewardService;
    @Override
    public void create(StewardRequestDto requestDto) {
        Steward steward = stewardMapper.toEntity(requestDto);
        stewardService.create(steward);
    }

    @Override
    @Transactional
    public StewardResponseDto getById(Integer id) {
        return stewardMapper.toResponseDto(stewardService.getById(id));
    }

    @Override
    @Transactional
    public List<StewardResponseDto> getAll() {
        return stewardMapper.toResponseDtoList(stewardService.getAll());
    }

    @Override
    public void update(Integer id, StewardRequestDto requestDto) {
        Steward steward = stewardMapper.toEntity(requestDto);
        stewardService.update(id, steward);
    }

    @Override
    public void delete(Integer id) {
        stewardService.delete(id);
    }

    @Override
    public boolean isStewardAvailableForTimeRange(Integer stewardId, LocalTime startTime, LocalTime endTime) {
        return stewardService.isStewardAvailableForTimeRange(stewardId, startTime, endTime);
    }

    @Override
    public StewardResponseDto changeAvailability(Integer id, boolean available) {
        return stewardMapper.toResponseDto(stewardService.changeAvailability(id, available));
    }

}
