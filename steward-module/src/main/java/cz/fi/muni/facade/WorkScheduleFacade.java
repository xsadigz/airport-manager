package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.WorkScheduleRequestDto;
import cz.fi.muni.api.responseDto.WorkScheduleResponseDto;
import cz.fi.muni.data.model.Steward;
import cz.fi.muni.data.model.WorkSchedule;
import cz.fi.muni.mapper.WorkScheduleMapper;
import cz.fi.muni.service.StewardService;
import cz.fi.muni.service.WorkScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WorkScheduleFacade implements IWorkScheduleFacade{

    private final WorkScheduleService workScheduleService;
    private final WorkScheduleMapper workScheduleMapper;

    private final StewardService stewardService;

    @Override
    public void create(WorkScheduleRequestDto requestDto) {
        WorkSchedule workSchedule = workScheduleMapper.toEntity(requestDto);
        List<Steward> stewards = new ArrayList<>();
        for (Integer id: requestDto.getStewardIds()){
            Steward steward = stewardService.getById(id);
            stewards.add(steward);
        }
        workSchedule.setStewards(stewards);
        workScheduleService.create(workSchedule);

    }

    @Override
    @Transactional
    public WorkScheduleResponseDto getById(Integer id) {
        return workScheduleMapper.toResponseDto(workScheduleService.getById(id));
    }

    @Override
    @Transactional
    public List<WorkScheduleResponseDto> getAll() {
        return workScheduleMapper.toResponseDtoList(workScheduleService.getAll());
    }

    @Override
    public void update(Integer id, WorkScheduleRequestDto requestDto) {
        WorkSchedule workSchedule = workScheduleMapper.toEntity(requestDto);
        workScheduleService.update(id, workSchedule);
    }

    @Override
    public void delete(Integer id) {
        workScheduleService.delete(id);
    }
}
