package cz.fi.muni.mapper;

import cz.fi.muni.api.requestDto.StewardRequestDto;
import cz.fi.muni.api.responseDto.StewardResponseDto;
import cz.fi.muni.data.model.Steward;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface StewardMapper {

    Steward toEntity(StewardRequestDto requestDto);

    StewardResponseDto toResponseDto(Steward destination);

    List<StewardResponseDto> toResponseDtoList(List<Steward> stewards);

}
