package cz.fi.muni.mapper;

import cz.fi.muni.api.requestDto.WorkDayRequestDto;
import cz.fi.muni.api.responseDto.WorkDayResponseDto;
import cz.fi.muni.api.responseDto.WorkScheduleResponseDto;
import cz.fi.muni.data.model.WorkDay;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface WorkDayMapper {

    WorkDay toEntity(WorkDayRequestDto requestDto);

    WorkDayResponseDto toResponseDto(WorkDay workDay);

    List<WorkScheduleResponseDto> toResponseDtoList(List<WorkDay> workDays);


}
