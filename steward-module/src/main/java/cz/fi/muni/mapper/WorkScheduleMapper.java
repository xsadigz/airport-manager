package cz.fi.muni.mapper;

import cz.fi.muni.api.requestDto.WorkScheduleRequestDto;
import cz.fi.muni.api.responseDto.WorkScheduleResponseDto;
import cz.fi.muni.data.model.WorkSchedule;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface WorkScheduleMapper {

    WorkSchedule toEntity(WorkScheduleRequestDto requestDto);

    WorkScheduleResponseDto toResponseDto(WorkSchedule workSchedule);

    List<WorkScheduleResponseDto> toResponseDtoList(List<WorkSchedule> workSchedules);


}
