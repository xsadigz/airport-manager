package cz.fi.muni.service;

import cz.fi.muni.api.responseDto.StewardResponseDto;
import cz.fi.muni.api.responseDto.WorkScheduleResponseDto;
import cz.fi.muni.data.model.Steward;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public interface IStewardService {

    Steward create(Steward steward);

    Steward getById(Integer id);

    List<Steward> getAll();

    Steward update(Integer id, Steward steward);

    void delete(Integer id);

    boolean isStewardAvailableForTimeRange(Integer stewardId, LocalTime startTime, LocalTime endTime);

    Steward changeAvailability(Integer id, boolean available);

}
