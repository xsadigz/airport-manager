package cz.fi.muni.service;

import cz.fi.muni.data.model.WorkSchedule;

import java.util.List;

public interface IWorkScheduleService {

    WorkSchedule create(WorkSchedule workSchedule);

    WorkSchedule getById(Integer id);

    List<WorkSchedule> getAll();

    WorkSchedule update(Integer id, WorkSchedule workSchedule);

    void delete(Integer id);

}
