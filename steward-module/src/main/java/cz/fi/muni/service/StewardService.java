package cz.fi.muni.service;

import cz.fi.muni.api.responseDto.StewardResponseDto;
import cz.fi.muni.api.responseDto.WorkScheduleResponseDto;
import cz.fi.muni.data.model.Steward;
import cz.fi.muni.data.repository.StewardRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StewardService implements IStewardService{
    private final StewardRepository  stewardRepository;

    @Override
    public Steward create(Steward steward) {
        return stewardRepository.save(steward);
    }

    @Override
    @Transactional
    public Steward getById(Integer id) {
        return stewardRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException(Steward.class, id));
    }

    @Override
    @Transactional
    public List<Steward> getAll() {
        return stewardRepository.findAll();
    }

    @Override
    public Steward update(Integer id, Steward steward) {
        Steward existingSteward = stewardRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException(Steward.class, id));
        existingSteward.setEmail(steward.getEmail());
        existingSteward.setFirstname(steward.getFirstname());
        existingSteward.setLastname(steward.getLastname());
        existingSteward.setAvailable(steward.isAvailable());
        existingSteward.setContactNumber(steward.getContactNumber());
        existingSteward.setWorkSchedule(steward.getWorkSchedule());
        return stewardRepository.save(existingSteward);
    }

    @Override
    public void delete(Integer id) {
        stewardRepository.deleteById(id);
    }

    @Override
    public boolean isStewardAvailableForTimeRange(Integer stewardId, LocalTime startTime, LocalTime endTime) {
        Steward steward = stewardRepository.findById(stewardId)
                .orElseThrow(() -> new EntityNotFoundException(Steward.class, stewardId));
        return steward.getWorkSchedule().getWorkDays().stream()
                .anyMatch(workDay -> workDay.getShiftStart().isBefore(startTime)
                        && workDay.getShiftEnd().isAfter(endTime));

    }

    @Override
    public Steward changeAvailability(Integer id, boolean available) {
        Steward steward = stewardRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Steward.class, id));
        steward.setAvailable(available);
        return stewardRepository.save(steward);
    }

}
