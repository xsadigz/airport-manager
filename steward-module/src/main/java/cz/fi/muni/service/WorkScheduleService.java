package cz.fi.muni.service;

import cz.fi.muni.data.model.WorkSchedule;
import cz.fi.muni.data.repository.WorkScheduleRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WorkScheduleService implements IWorkScheduleService{

    private final WorkScheduleRepository workScheduleRepository;

    @Override
    public WorkSchedule create(WorkSchedule workSchedule) {
        return workScheduleRepository.save(workSchedule);
    }

    @Override
    @Transactional
    public WorkSchedule getById(Integer id) {
        return workScheduleRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(WorkSchedule.class, id));
    }

    @Override
    @Transactional
    public List<WorkSchedule> getAll() {
        return workScheduleRepository.findAll();
    }

    @Override
    public WorkSchedule update(Integer id, WorkSchedule workSchedule) {
        WorkSchedule exist = workScheduleRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(WorkSchedule.class, id));
        exist.setTemporary(workSchedule.isTemporary());
        exist.setStewards(workSchedule.getStewards());
        exist.setWorkDays(workSchedule.getWorkDays());
        return workScheduleRepository.save(exist);
    }

    @Override
    public void delete(Integer id) {
        workScheduleRepository.deleteById(id);
    }

}
