INSERT INTO work_schedule (id, temporary) VALUES (3, false);

INSERT INTO work_days (schedule_id, day, shift_start, shift_end, break_hour) VALUES (1, 'MONDAY', TIME '08:00:00', TIME '16:00:00', 1);
INSERT INTO work_days (schedule_id, day, shift_start, shift_end, break_hour) VALUES (1, 'TUESDAY', '08:00:00', '16:00:00', 1);

INSERT INTO steward (id, firstname, lastname, contact_number, email, work_schedule_id, available) VALUES (2, 'John', 'Doe', '123456789', 'john.doe@example.com', 1, true);