package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.StewardRequestDto;
import cz.fi.muni.api.responseDto.StewardResponseDto;
import cz.fi.muni.facade.StewardFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

public class StewardControllerTest {

    @Mock
    private StewardFacade stewardFacade;

    @InjectMocks
    private StewardController stewardController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void addStewardTest() {
        StewardRequestDto requestDto = new StewardRequestDto();
        requestDto.setFirstname("John");
        requestDto.setLastname("Doe");
        requestDto.setContactNumber("123456789");
        requestDto.setEmail("john.doe@example.com");
        stewardController.addSteward(requestDto);

        verify(stewardFacade, times(1)).create(requestDto);
    }

    @Test
    public void getStewardTest() {
        int id = 1;
        StewardResponseDto responseDto = new StewardResponseDto();
        when(stewardFacade.getById(id)).thenReturn(responseDto);

        ResponseEntity<StewardResponseDto> responseEntity = stewardController.getSteward(id);

        verify(stewardFacade, times(1)).getById(id);
        assert responseEntity.getStatusCode() == HttpStatus.OK;
        assert responseEntity.getBody() == responseDto;
    }

    @Test
    public void getAllStewardsTest() {
        List<StewardResponseDto> responseDtoList = Collections.emptyList();
        when(stewardFacade.getAll()).thenReturn(responseDtoList);

        ResponseEntity<List<StewardResponseDto>> responseEntity = stewardController.getAllStewards();

        verify(stewardFacade, times(1)).getAll();
        assert responseEntity.getStatusCode() == HttpStatus.OK;
        assert responseEntity.getBody() == responseDtoList;
    }

    @Test
    public void updateStewardTest() {
        int id = 1;
        StewardRequestDto requestDto = new StewardRequestDto();
        requestDto.setFirstname("John");
        requestDto.setLastname("Doe");
        requestDto.setContactNumber("123456789");
        requestDto.setEmail("john.doe@example.com");


        stewardController.editSteward(id, requestDto);

        verify(stewardFacade, times(1)).update(id, requestDto);
    }

    @Test
    public void deleteStewardTest() {
        int id = 1;

        stewardController.deleteSteward(id);

        verify(stewardFacade, times(1)).delete(id);
    }
}
