package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.WorkDayRequestDto;
import cz.fi.muni.api.requestDto.WorkScheduleRequestDto;
import cz.fi.muni.api.responseDto.StewardResponseDto;
import cz.fi.muni.api.responseDto.WorkDayResponseDto;
import cz.fi.muni.api.responseDto.WorkScheduleResponseDto;
import cz.fi.muni.data.model.DayOfWeek;
import cz.fi.muni.facade.IWorkScheduleFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

public class WorkScheduleControllerTest {

    @Mock
    private IWorkScheduleFacade workScheduleFacade;

    @InjectMocks
    private WorkScheduleController workScheduleController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void addWorkScheduleTest() {
        WorkDayRequestDto workDayRequestDto = new WorkDayRequestDto();
        workDayRequestDto.setDay(DayOfWeek.MONDAY);
        workDayRequestDto.setShiftStart(LocalTime.now());
        workDayRequestDto.setShiftEnd(LocalTime.now().plusHours(8));
        workDayRequestDto.setBreakHour(1);

        WorkScheduleRequestDto requestDto = new WorkScheduleRequestDto();
        requestDto.setWorkDays(Collections.singletonList(workDayRequestDto));
        requestDto.setTemporary(false);
        requestDto.setStewardIds(Arrays.asList(1, 2, 3));

        workScheduleController.addWorkSchedule(requestDto);

        verify(workScheduleFacade, times(1)).create(requestDto);
    }

    @Test
    public void deleteWorkScheduleTest() {
        int id = 1;

        workScheduleController.deleteWorkSchedule(id);

        verify(workScheduleFacade, times(1)).delete(id);
    }

    @Test
    public void getAllWorkSchedulesTest() {
        StewardResponseDto stewardResponseDto = new StewardResponseDto();
        stewardResponseDto.setId(1);
        stewardResponseDto.setFirstname("John");
        stewardResponseDto.setLastname("Doe");
        stewardResponseDto.setContactNumber("123456789");
        stewardResponseDto.setEmail("john.doe@example.com");
        stewardResponseDto.setAvailable(true);

        WorkDayResponseDto workDayResponseDto = new WorkDayResponseDto();
        workDayResponseDto.setDay(DayOfWeek.MONDAY);
        workDayResponseDto.setShiftStart(LocalTime.now());
        workDayResponseDto.setShiftEnd(LocalTime.now().plusHours(8));
        workDayResponseDto.setBreakHour(1);

        WorkScheduleResponseDto responseDto = new WorkScheduleResponseDto();
        responseDto.setWorkDays(Collections.singletonList(workDayResponseDto));
        responseDto.setTemporary(false);

        List<WorkScheduleResponseDto> responseDtoList = Collections.singletonList(responseDto);
        when(workScheduleFacade.getAll()).thenReturn(responseDtoList);

        ResponseEntity<List<WorkScheduleResponseDto>> responseEntity = workScheduleController.getAllWorkSchedules();

        verify(workScheduleFacade, times(1)).getAll();
        assert responseEntity.getStatusCode() == HttpStatus.OK;
        assert responseEntity.getBody() == responseDtoList;
    }

    @Test
    public void getWorkScheduleTest() {
        int id = 1;
        StewardResponseDto stewardResponseDto = new StewardResponseDto();
        stewardResponseDto.setId(1);
        stewardResponseDto.setFirstname("John");
        stewardResponseDto.setLastname("Doe");
        stewardResponseDto.setContactNumber("123456789");
        stewardResponseDto.setEmail("john.doe@example.com");
        stewardResponseDto.setAvailable(true);

        WorkDayResponseDto workDayResponseDto = new WorkDayResponseDto();
        workDayResponseDto.setDay(DayOfWeek.MONDAY);
        workDayResponseDto.setShiftStart(LocalTime.now());
        workDayResponseDto.setShiftEnd(LocalTime.now().plusHours(8));
        workDayResponseDto.setBreakHour(1);

        WorkScheduleResponseDto responseDto = new WorkScheduleResponseDto();
        responseDto.setWorkDays(Collections.singletonList(workDayResponseDto));
        responseDto.setTemporary(false);

        when(workScheduleFacade.getById(id)).thenReturn(responseDto);

        ResponseEntity<WorkScheduleResponseDto> responseEntity = workScheduleController.getWorkSchedule(id);

        verify(workScheduleFacade, times(1)).getById(id);
        assert responseEntity.getStatusCode() == HttpStatus.OK;
        assert responseEntity.getBody() == responseDto;
    }

    @Test
    public void editWorkScheduleTest() {
        int id = 1;
        WorkDayRequestDto workDayRequestDto = new WorkDayRequestDto();
        workDayRequestDto.setDay(DayOfWeek.MONDAY);
        workDayRequestDto.setShiftStart(LocalTime.now());
        workDayRequestDto.setShiftEnd(LocalTime.now().plusHours(8));
        workDayRequestDto.setBreakHour(1);

        WorkScheduleRequestDto requestDto = new WorkScheduleRequestDto();
        requestDto.setWorkDays(Collections.singletonList(workDayRequestDto));
        requestDto.setTemporary(false);
        requestDto.setStewardIds(Arrays.asList(1, 2, 3));

        workScheduleController.editWorkSchedule(id, requestDto);

        verify(workScheduleFacade, times(1)).update(id, requestDto);
    }
}