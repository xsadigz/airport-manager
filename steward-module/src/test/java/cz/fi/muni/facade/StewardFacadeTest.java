package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.StewardRequestDto;
import cz.fi.muni.api.responseDto.StewardResponseDto;
import cz.fi.muni.data.model.Steward;
import cz.fi.muni.mapper.StewardMapper;
import cz.fi.muni.service.StewardService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

class StewardFacadeTest {

    @Mock
    private StewardMapper stewardMapper;

    @Mock
    private StewardService stewardService;

    @InjectMocks
    private StewardFacade stewardFacade;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createTest() {
        // Given
        StewardRequestDto requestDto = new StewardRequestDto();
        requestDto.setFirstname("John");
        requestDto.setLastname("Doe");
        requestDto.setContactNumber("123456789");
        requestDto.setEmail("john.doe@example.com");

        Steward steward = new Steward();
        when(stewardMapper.toEntity(requestDto)).thenReturn(steward);

        // When
        stewardFacade.create(requestDto);

        // Then
        verify(stewardMapper).toEntity(requestDto);
        verify(stewardService).create(steward);
    }

    @Test
    void getByIdTest() {
        // Given
        int id = 1;
        Steward steward = new Steward();
        StewardResponseDto responseDto = new StewardResponseDto();
        when(stewardService.getById(id)).thenReturn(steward);
        when(stewardMapper.toResponseDto(steward)).thenReturn(responseDto);

        // When
        StewardResponseDto result = stewardFacade.getById(id);

        // Then
        verify(stewardService).getById(id);
        verify(stewardMapper).toResponseDto(steward);
        assert result == responseDto;
    }

    @Test
    void getAllTest() {
        // Given
        List<Steward> stewards = Collections.singletonList(new Steward());
        List<StewardResponseDto> responseDtoList = Collections.singletonList(new StewardResponseDto());
        when(stewardService.getAll()).thenReturn(stewards);
        when(stewardMapper.toResponseDtoList(stewards)).thenReturn(responseDtoList);

        // When
        List<StewardResponseDto> result = stewardFacade.getAll();

        // Then
        verify(stewardService).getAll();
        verify(stewardMapper).toResponseDtoList(stewards);
        assert result == responseDtoList;
    }

    @Test
    void updateTest() {
        // Given
        int id = 1;
        StewardRequestDto requestDto = new StewardRequestDto();
        requestDto.setFirstname("John");
        requestDto.setLastname("Doe");
        requestDto.setContactNumber("123456789");
        requestDto.setEmail("john.doe@example.com");

        Steward steward = new Steward();
        when(stewardMapper.toEntity(requestDto)).thenReturn(steward);

        // When
        stewardFacade.update(id, requestDto);

        // Then
        verify(stewardMapper).toEntity(requestDto);
        verify(stewardService).update(id, steward);
    }

    @Test
    void deleteTest() {
        // Given
        int id = 1;

        // When
        stewardFacade.delete(id);

        // Then
        verify(stewardService).delete(id);
    }
}
