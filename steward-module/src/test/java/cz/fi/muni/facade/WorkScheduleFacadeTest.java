package cz.fi.muni.facade;

import cz.fi.muni.api.requestDto.WorkDayRequestDto;
import cz.fi.muni.api.requestDto.WorkScheduleRequestDto;
import cz.fi.muni.api.responseDto.WorkScheduleResponseDto;
import cz.fi.muni.data.model.DayOfWeek;
import cz.fi.muni.data.model.Steward;
import cz.fi.muni.data.model.WorkSchedule;
import cz.fi.muni.mapper.WorkScheduleMapper;
import cz.fi.muni.service.StewardService;
import cz.fi.muni.service.WorkScheduleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

public class WorkScheduleFacadeTest {

    @Mock
    private WorkScheduleMapper workScheduleMapper;

    @Mock
    private WorkScheduleService workScheduleService;

    @Mock
    private StewardService stewardService;

    @InjectMocks
    private WorkScheduleFacade workScheduleFacade;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        workScheduleFacade = new WorkScheduleFacade(workScheduleService, workScheduleMapper, stewardService);
    }

    @Test
    public void createWorkScheduleTest() {
        WorkDayRequestDto workDayRequestDto = new WorkDayRequestDto();
        workDayRequestDto.setDay(DayOfWeek.MONDAY);
        workDayRequestDto.setShiftStart(LocalTime.now());
        workDayRequestDto.setShiftEnd(LocalTime.now().plusHours(8));
        workDayRequestDto.setBreakHour(1);

        WorkScheduleRequestDto requestDto = new WorkScheduleRequestDto();
        requestDto.setWorkDays(Collections.singletonList(workDayRequestDto));
        requestDto.setTemporary(false);
        requestDto.setStewardIds(Arrays.asList(1));

        WorkSchedule workSchedule = new WorkSchedule();
        Steward steward = new Steward();
        when(stewardService.getById(anyInt())).thenReturn(steward);
        when(workScheduleMapper.toEntity(any(WorkScheduleRequestDto.class))).thenReturn(workSchedule);

        workScheduleFacade.create(requestDto);

        verify(workScheduleService, times(1)).create(any(WorkSchedule.class));
    }

    @Test
    public void getByIdTest() {
        int id = 1;
        WorkSchedule workSchedule = new WorkSchedule();
        WorkScheduleResponseDto responseDto = new WorkScheduleResponseDto();
        when(workScheduleService.getById(id)).thenReturn(workSchedule);
        when(workScheduleMapper.toResponseDto(workSchedule)).thenReturn(responseDto);

        WorkScheduleResponseDto result = workScheduleFacade.getById(id);

        verify(workScheduleService).getById(id);
        verify(workScheduleMapper).toResponseDto(workSchedule);
        assert result == responseDto;
    }

    @Test
    public void getAllTest() {
        List<WorkSchedule> workSchedules = Collections.singletonList(new WorkSchedule());
        List<WorkScheduleResponseDto> responseDtoList = Collections.singletonList(new WorkScheduleResponseDto());
        when(workScheduleService.getAll()).thenReturn(workSchedules);
        when(workScheduleMapper.toResponseDtoList(workSchedules)).thenReturn(responseDtoList);

        List<WorkScheduleResponseDto> result = workScheduleFacade.getAll();

        verify(workScheduleService).getAll();
        verify(workScheduleMapper).toResponseDtoList(workSchedules);
        assert result == responseDtoList;
    }

    @Test
    public void updateTest() {
        int id = 1;
        WorkDayRequestDto workDayRequestDto = new WorkDayRequestDto();
        workDayRequestDto.setDay(DayOfWeek.MONDAY);
        workDayRequestDto.setShiftStart(LocalTime.now());
        workDayRequestDto.setShiftEnd(LocalTime.now().plusHours(8));
        workDayRequestDto.setBreakHour(1);

        WorkScheduleRequestDto requestDto = new WorkScheduleRequestDto();
        requestDto.setWorkDays(Collections.singletonList(workDayRequestDto));
        requestDto.setTemporary(false);
        requestDto.setStewardIds(Arrays.asList(1, 2, 3));

        WorkSchedule workSchedule = new WorkSchedule();
        when(workScheduleMapper.toEntity(requestDto)).thenReturn(workSchedule);

        workScheduleFacade.update(id, requestDto);

        verify(workScheduleMapper).toEntity(requestDto);
        verify(workScheduleService).update(id, workSchedule);
    }

    @Test
    public void deleteTest() {
        int id = 1;

        workScheduleFacade.delete(id);

        verify(workScheduleService).delete(id);
    }
}