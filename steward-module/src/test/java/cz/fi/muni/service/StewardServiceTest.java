package cz.fi.muni.service;

import cz.fi.muni.data.model.Steward;
import cz.fi.muni.data.repository.StewardRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.webjars.NotFoundException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class StewardServiceTest {

    @Mock
    private StewardRepository stewardRepository;

    @InjectMocks
    private StewardService stewardService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void createTest() {
        Steward steward = new Steward();
        when(stewardRepository.save(any(Steward.class))).thenReturn(steward);

        Steward result = stewardService.create(steward);

        verify(stewardRepository, times(1)).save(steward);
        assertEquals(steward, result);
    }

    @Test
    public void getByIdTest() {
        int id = 1;
        Steward steward = new Steward();
        when(stewardRepository.findById(id)).thenReturn(Optional.of(steward));

        Steward result = stewardService.getById(id);

        verify(stewardRepository, times(1)).findById(id);
        assertEquals(steward, result);
    }

    @Test
    public void getByIdNotFoundTest() {
        int id = 1;
        when(stewardRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> stewardService.getById(id));

        verify(stewardRepository, times(1)).findById(id);
    }

    @Test
    public void getAllTest() {
        List<Steward> stewards = Collections.singletonList(new Steward());
        when(stewardRepository.findAll()).thenReturn(stewards);

        List<Steward> result = stewardService.getAll();

        verify(stewardRepository, times(1)).findAll();
        assertEquals(stewards, result);
    }

    @Test
    public void updateTest() {
        int id = 1;
        Steward steward = new Steward();
        when(stewardRepository.findById(id)).thenReturn(Optional.of(steward));
        when(stewardRepository.save(any(Steward.class))).thenReturn(steward);

        Steward result = stewardService.update(id, steward);

        verify(stewardRepository, times(1)).findById(id);
        verify(stewardRepository, times(1)).save(steward);
        assertEquals(steward, result);
    }

    @Test
    public void deleteTest() {
        int id = 1;

        stewardService.delete(id);

        verify(stewardRepository, times(1)).deleteById(id);
    }


}