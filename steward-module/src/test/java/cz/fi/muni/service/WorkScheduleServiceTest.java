package cz.fi.muni.service;

import cz.fi.muni.data.model.WorkSchedule;
import cz.fi.muni.data.repository.WorkScheduleRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class WorkScheduleServiceTest {

    @Mock
    private WorkScheduleRepository workScheduleRepository;

    @InjectMocks
    private WorkScheduleService workScheduleService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void createTest() {
        WorkSchedule workSchedule = new WorkSchedule();
        when(workScheduleRepository.save(any(WorkSchedule.class))).thenReturn(workSchedule);

        WorkSchedule result = workScheduleService.create(workSchedule);

        verify(workScheduleRepository, times(1)).save(workSchedule);
        assertEquals(workSchedule, result);
    }

    @Test
    public void getByIdTest() {
        int id = 1;
        WorkSchedule workSchedule = new WorkSchedule();
        when(workScheduleRepository.findById(id)).thenReturn(Optional.of(workSchedule));

        WorkSchedule result = workScheduleService.getById(id);

        verify(workScheduleRepository, times(1)).findById(id);
        assertEquals(workSchedule, result);
    }

    @Test
    public void getByIdNotFoundTest() {
        int id = 1;
        when(workScheduleRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> workScheduleService.getById(id));

        verify(workScheduleRepository, times(1)).findById(id);
    }

    @Test
    public void getAllTest() {
        List<WorkSchedule> workSchedules = Collections.singletonList(new WorkSchedule());
        when(workScheduleRepository.findAll()).thenReturn(workSchedules);

        List<WorkSchedule> result = workScheduleService.getAll();

        verify(workScheduleRepository, times(1)).findAll();
        assertEquals(workSchedules, result);
    }

    @Test
    public void updateTest() {
        int id = 1;
        WorkSchedule workSchedule = new WorkSchedule();
        when(workScheduleRepository.findById(id)).thenReturn(Optional.of(workSchedule));
        when(workScheduleRepository.save(any(WorkSchedule.class))).thenReturn(workSchedule);

        WorkSchedule result = workScheduleService.update(id, workSchedule);

        verify(workScheduleRepository, times(1)).findById(id);
        verify(workScheduleRepository, times(1)).save(workSchedule);
        assertEquals(workSchedule, result);
    }

    @Test
    public void deleteTest() {
        int id = 1;

        workScheduleService.delete(id);

        verify(workScheduleRepository, times(1)).deleteById(id);
    }
}